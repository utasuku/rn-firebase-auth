import React from 'react';
import { Text, View, TouchableOpacity, TouchableHighlight, StyleSheet } from 'react-native';
import { Constants, Camera, FileSystem, Permissions } from 'expo';

import { Container, Content, Header, Item, Icon, Input, Button } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class CameraScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  }

  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  };

  constructor (props) {
    super(props);

    this.state = {
      newPhotos: false,
    }

    this.camera = null
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });

    FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + 'photos').catch(e => {
      console.log(e, 'Directory exists');
    });
  }

  snap = async () => {
    console.log('snap!!');
    console.log(this.camera != null);
    if (this.camera) {
      this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved });
    }
  };

  onPictureSaved = async (photo) => {
    await FileSystem.moveAsync({
      from: photo.uri,
      to: `${FileSystem.documentDirectory}photos/${Date.now()}.jpg`,
    });

    console.log(photo)
    this.setState({ newPhotos: true });
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera ref={ref => { this.camera = ref; }}
            style={{ flex: 1, justifyContent: 'space-between' }} type={this.state.type}>

            <Header searchBar rounded
                style={{
                    position: 'absolute', backgroundColor: 'transparent',
                    left: 0, top: 0, right: 0, zIndex: 100, alignItems: 'center'
                }}
            >
                <View style={{ flexDirection: 'row', flex: 2, justifyContent: 'space-around' }}>
                    <Icon name="ios-flash" style={{ color: 'white', fontWeight: 'bold' }} />
                    <Icon
                        onPress={() => {
                            this.setState({
                                type: this.state.type === Camera.Constants.Type.back ?
                                    Camera.Constants.Type.front :
                                    Camera.Constants.Type.back
                            })
                        }}
                        name="ios-reverse-camera" style={{ color: 'white', fontWeight: 'bold' }} />
                </View>
            </Header>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between',
                paddingHorizontal: 10, marginBottom: 15, alignItems: 'flex-end',
                backgroundColor: 'transparent' }}>
                <MaterialCommunityIcons name="message-reply"
                    style={{ color: 'white', fontSize: 36, zIndex: 10 }}
                ></MaterialCommunityIcons>

                <View style={{ alignItems: 'center' }}>
                  <TouchableHighlight onPress={() => this.snap()}>
                    <MaterialCommunityIcons name="circle-outline"
                        style={{ color: 'white', fontSize: 100, zIndex: 10 }}
                    ></MaterialCommunityIcons>
                  </TouchableHighlight>
                  <Icon name="ios-images" style={{ color: 'white', fontSize: 36, zIndex: 10 }} />
                </View>
                <MaterialCommunityIcons name="google-circles-communities"
                    style={{ color: 'white', fontSize: 36, zIndex: 10 }}
                ></MaterialCommunityIcons>

            </View>

          </Camera>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    position: 'absolute',
    backgroundColor: 'transparent',
    left: 0,
    top: 10,
    right: 0,
    zIndex: 100
  },
  cameraView: {
    flexDirection: 'row',
    flex: 4,
  }
})
