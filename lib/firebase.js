import * as firebase from 'firebase';
// import "firebase/app";
import "firebase/firestore";
// import "firebase/functions";

const firebaseConfig = {
  apiKey: "AIzaSyCM7_KvcuhKm8qUJdKKAgEsUsaeDs_jM4s",
  authDomain: "ufami-1b0ff.firebaseapp.com",
  databaseURL: "https://ufami-1b0ff.firebaseio.com",
  projectId: "ufami-1b0ff",
  storageBucket: "ufami-1b0ff.appspot.com",
  messagingSenderId: "906311716970"
}

export default class Firebase {
  static initApp;
  static firestore: firebase.firestore.Firestore;
  static auth: firebase.auth.Auth;
  static database: firebase.database;
  // static storage: firebase.storage.Storage;

  static init() {
    Firebase.initApp = firebase.initializeApp(firebaseConfig);
    Firebase.firestore = firebase.firestore();
    Firebase.auth = firebase.auth();
    Firebase.database = firebase.database;
    // Firebase.storage = firebase.storage();

    Firebase.firestore.settings({ timestampsInSnapshots: true });
  }

  static isLogged () {
    if (firebase.auth().currentUser) {
      return true
    }

    return false
  }

  static getLoadUser () {
    return firebase.auth().currentUser
  }
}
