import Firebase from './firebase'
import { AsyncStorage } from "react-native";

export default class Auth {
  static restricted () {
    if (Firebase.isLogged()) {
      return true
    }

    return false
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        return value
      }
     } catch (error) {
       console.log('An error has been ocurred.')
     }

     return null
  }
}
