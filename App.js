import { Root } from "native-base"

import React from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { createSwitchNavigator, createStackNavigator } from 'react-navigation'
import { Icon, Text } from 'native-base'

import Firebase from './lib/firebase'
import LoginScreen from './app/LoginScreen'
import AppDrawerNavigator from './app/DrawerNavigator'

const AuthStackNavigator = createStackNavigator({
  Login: LoginScreen,
})

const AppSwitchNavigator = createSwitchNavigator({
  Auth: AuthStackNavigator,
  App: AppDrawerNavigator
}, {
  navigationOptions: {
    gesturesEnabled: false
  }
})

export default class App extends React.Component {
  constructor (props) {
    super(props)

    console.ignoredYellowBox = ['Setting a timer'];
    Firebase.init()
  }

  render() {
    return (
      <Root>
        <AppSwitchNavigator/>
      </Root>
    )
  }
}
