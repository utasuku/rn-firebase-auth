import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  BackHandler
} from "react-native";

import Firebase from '../../lib/firebase'
import { Container, Header, Item, Right, Icon, Text, Fab, Toast,
  Form, Label, Input, DatePicker, Textarea } from 'native-base'


export default class ScreenOne extends Component {
  constructor (props) {
    super(props)
  }

  _retrieveData = () => {

  }

  _getData = () => {

  }

  _onUpdate = () => {

  }

  render() {
    return (
      <Container>
        <Text>Screen One</Text>
      </Container>
    );
  }

  componentDidMount() {

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
  }
});
