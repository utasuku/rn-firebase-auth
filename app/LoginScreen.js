import React, { Component } from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableHighlight,
  ScrollView,
  AsyncStorage,
} from 'react-native';

import Firebase from '../lib/firebase';

import { Input } from '../components/Input';
import { Button } from '../components/Button';

export default class LoginScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor (props) {
    super(props);

    this.state = {
      loginView: true,
      authenticating: false,
      authenticated: false,
      creating: false,
      email: '',
      name: '',
      lastname: '',
      password: '',
      repeat: '',
      user: null,
      errorLogin: '',
      errorCreate: '',
      response: '',
    }
  }

  resetState () {
    this.setState({
      authenticating: false,
      creating: false,
      email: '',
      name: '',
      lastname: '',
      password: '',
      repeat: '',
      user: null,
      errorLogin: '',
      errorCreate: '',
      response: '',
    })
  }

  _storeData = async (data) => {
    let user = {
      name: data.displayName,
      email: data.email,
      photo: data.photoURL,
      uid: data.uid,
    }

    try {
      await AsyncStorage.setItem('_lsUser', JSON.stringify(user));
    } catch (error) {
      this.setState({
        authenticating: false,
        user: null,
        errorLogin: 'Hubo un problema, intente de nuevo',
      })
    }
  }

  onPressSignIn() {
    this.setState({authenticating: true});

    const { email, password } = this.state;

    Firebase.initApp.auth().signInWithEmailAndPassword(email, password)
    .then(user => {
      console.log(user)
      this._storeData(user.user).then(() => {
        this.setState({
          authenticating: false,
          authenticated: true
        })
        this.props.navigation.navigate('App')
      })
    })
    .catch(() => this.setState({
      authenticating: false,
      user: null,
      errorLogin: 'Usuario y/o contraseña incorrectos',
    }))
  }

  signUp () {
    this.setState({creating: true});

    const { email, password, repeat, name, lastname } = this.state;

    if (email.trim() === '' ||
      password.trim() === '' ||
      repeat.trim() === '' ||
      name.trim() === '' ||
      lastname.trim() === ''
    ) {
      this.setState({
        creating: false,
        errorCreate: 'Todos los campos son obligatorios'
      })

      return false
    }

    Firebase.initApp.auth().createUserWithEmailAndPassword(email, password)
    .then(user => {
      this.resetState()
      this.props.navigation.navigate('Drawer')
    })
    .catch((err) => this.setState({
      creating: false,
      user: null,
      errorCreate: 'No se pudo crear la cuenta',
    }))
  }

  renderLoading() {
    if (this.state.authenticating) {
      return (
        <ActivityIndicator size="large" />
      );
    }

    return (
      <Button onPress={() => this.onPressSignIn() }>Iniciar sesión</Button>
    )
  }

  renderCreating () {
    if (this.state.creating) {
      return (
        <ActivityIndicator size="large" />
      );
    }

    return (
      <Button onPress={() => this.signUp() }>Crear cuenta</Button>
    )
  }

  toogleRender (render) {
    this.setState({
      loginView: render
    })
  }

  renderCurrentState(){
    if (this.state.loginView) {
      return (
        <KeyboardAvoidingView style={styles.form} behavior="padding" enabled>
           <ScrollView style={styles.scroll}>
            <Image
              source={require('../assets/mf_small.png')}/>
            <Input
              placeholder=''
              label='Correo electrónico'
              onChangeText={email => this.setState({email})}
              value={this.state.email}
            />
            <Input
              placeholder=''
              label='Contraseña'
              secureTextEntry
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
            {this.renderLoading()}
            <Text style={styles.error}>{this.state.errorLogin}</Text>
            <View style={styles.account}>
              <TouchableHighlight style={styles.btnLine}
                onPress={() => this.props.navigation.navigate('Drawer') }>
                <Text style={styles.btnLineText}>¿Olvidaste tu contraseña?</Text>
              </TouchableHighlight>
              <TouchableHighlight style={styles.btnLine}
                onPress={() => this.toogleRender(false) }>
                <Text style={styles.btnLineText}>Crear cuenta nueva</Text>
              </TouchableHighlight>
            </View>
           </ScrollView>
        </KeyboardAvoidingView>
      )
    } else {
      return (
        <KeyboardAvoidingView style={styles.form} behavior="padding" enabled>
          <ScrollView style={styles.scroll}>
            <Image source={require('../assets/mf_small.png')}/>
            <Input
              placeholder=''
              label='Correo electrónico'
              onChangeText={email => this.setState({email})}
              value={this.state.email}
            />
            <Input
              placeholder=''
              label='Nombre'
              onChangeText={name => this.setState({name})}
              value={this.state.name}
            />
            <Input
              placeholder=''
              label='Apellidos'
              onChangeText={lastname => this.setState({lastname})}
              value={this.state.lastname}
            />
            <Input
              placeholder=''
              label='Contraseña'
              secureTextEntry
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
            <Input
              placeholder=''
              label='Repetir contraseña'
              secureTextEntry
              onChangeText={repeat => this.setState({repeat})}
              value={this.state.repeat}
            />
            {this.renderCreating()}
            <Text style={styles.error}>{this.state.errorCreate}</Text>
            <View style={styles.account}>
              <TouchableHighlight style={styles.btnLine}
                onPress={() => this.toogleRender(true) }>
                <Text style={styles.btnLineText}>Ya tengo una cuenta</Text>
              </TouchableHighlight>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      )
    }
  }

  UNSAFE_componentWillMount () {
    if (Firebase.auth.currentUser) {
      this.props.navigation.navigate('Drawer')
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderCurrentState()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  scroll: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 40,
  },
  form: {
    flex: 1,
  },
  logo: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: 100,
  },
  logoImg: {
    width: 200,
  },
  error: {
    color: '#F00',
    textAlign: 'center',
    padding: 10,
  },
  account: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 40,
  },
  btnLine: {
    marginTop: 0,
    marginBottom: 15,
  },
  btnLineText: {
    color: '#00aeef',
  }
});
