import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableHighlight
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Firebase from '../lib/firebase';

export default class SettingsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Ajustes',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  logout () {
    Firebase.auth.signOut()
    .then(() => {
      console.log('Closing...')
      this.props.navigation.navigate('Login')
    }, error => {
      console.error('Sign Out Error', error);
    });
  }

  render () {
    return (
      <View styles={styles.container}>
        <TouchableHighlight onPress={() => this.props.navigation.push('Privacy') }>
          <Text style={styles.item}><Ionicons style={styles.icon} name="md-person" size={18} /> Privacidad</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => this.props.navigation.push('Notification') }>
          <Text style={styles.item}><Ionicons style={styles.icon} name="md-notifications" size={18} /> Notificaciones</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => this.logout() }>
          <Text style={styles.item}><Ionicons style={styles.icon} name="md-log-out" size={18} /> Cerrar sesión</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  list: {
    backgroundColor: '#FFF'
  },
  item: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    fontSize: 18,
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#FFF',
  },
  icon: {
    paddingRight: 15,
  }
})
