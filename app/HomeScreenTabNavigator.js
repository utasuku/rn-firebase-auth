import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight
} from "react-native";

import { createBottomTabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Icon } from 'native-base'


import ScreenOne from './tab-navigator/ScreenOne'
import ScreenTwo from './tab-navigator/ScreenTwo'

export default class AppTabNavigator extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Inicio',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      ),
      headerRight: (
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('search')}>
            <Icon type="MaterialIcons" name="search"></Icon>
          </TouchableHighlight>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('more')}>
            <Icon type="MaterialIcons" name="more-vert"></Icon>
          </TouchableHighlight>
        </View>
      ),
    }
  }

  render() {
    return (
      <HomeScreenTabNavigator screenProps={{ navigation: this.props.navigation }} />
    )
  }

  searchOpen = () => {
    this.props.navigation.navigate('Search')
  }

  componentDidMount () {
    this.props.navigation.setParams({ search: this.searchOpen })
  }
}

const HomeScreenTabNavigator = createBottomTabNavigator({
    ScreenOne: {
        screen: ScreenOne,
        navigationOptions: {
            tabBarLabel: 'Feed',
            tabBarIcon: () => (
                <Ionicons name="md-compass" size={24} />
            )
        }
    },
    ScreenTwo: {
        screen: ScreenTwo,
        navigationOptions: {
            tabBarLabel: 'Feed',
            tabBarIcon: () => (
                <Ionicons name="md-compass" size={24} />
            )
        }
    }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F00'
  },
});
