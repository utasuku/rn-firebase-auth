import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Alert,
  TouchableHighlight,
} from 'react-native'

import Firebase from '../../lib/firebase'
import * as moment from 'moment-timezone'

import { Container, Content, Icon, Text, Picker, Fab, Toast,
  Form, Label, Input, Item, DatePicker, Textarea } from 'native-base'

export default class LabsAddScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Nuevo estudio',
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      loading: true,
      userLogged: null,
      saving: false,

      title: '',
      price: '',
      description: '',
      date: null,
    }

    this.setDate = this.setDate.bind(this)
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser')
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
    } catch (error) {
    // Error retrieving data
    }
  }

  _getData = () => {

  }

  _onUpdate = () => {

  }

  setDate (newDate) {
    this.setState({
      date: newDate,
    })
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <Form>
            <Item stackedLabel>
              <Label>Estudio de laboratorio:</Label>
              <Input maxLength = {50}
                onChangeText={title => this.setState({title})}>{this.state.title}</Input>
            </Item>
            <Item stackedLabel>
              <Label>Precio:</Label>
              <Input maxLength = {5}
                onChangeText={price => this.setState({price})}>{this.state.price}</Input>
            </Item>
            <Item>
              <Label>Fecha: </Label>
              <DatePicker
                defaultDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={'Selecciona la fecha'}
                textStyle={{ color: "green", textAlign: 'left' }}
                placeHolderTextStyle={{ color: "#d3d3d3", textAlign: 'left' }}
                onDateChange={this.setDate}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Descripción:</Label>
              <Textarea style={{width: '100%', marginTop: 10, marginBottom: 10}}
                onChangeText={description => this.setState({description})}
                bordered rowSpan={7}  value={this.state.description} />
            </Item>
          </Form>
        </Content>
        <Fab
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            // this._onCreate()
          }}>
          <Icon type="MaterialIcons" name="done" />
        </Fab>
      </Container>
    )
  }

  componentDidMount() {
    this._retrieveData().then(() => {
      this._getData()
    })
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
    flex: 1,
  },
})

