import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  BackHandler,
  ActivityIndicator
} from "react-native";

import Firebase from '../../lib/firebase';

import { Button } from '../../components/Button';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Container, Content, Card, CardItem, Body, Text, H1, H2, H3,
  Form, Item, Input, Label, Textarea, Icon, DatePicker, Thumbnail, ListItem,
  Left, Right, Radio, Switch } from 'native-base';

export default class HistoryAddScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Nuevo registro'
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      loading: true,
      loadingSave: false,
      userLogged: null,
      switchVal: false,

      title: '',
      hospital: '',
      date_entrance: '',
      description: '',
      spend_extra: '',
      level: [
        true,
        false,
        false,
        false
      ],
      urgency: 1,
      family_name: '',
      active: false,
      prescription: {
        doctor_name: '',
        description: '',
        created_at: '',
        updated_at: '',
        user_create: '',
        user_update: ''
      },
      laboratories: [],
      spends: [],

      user_patient: '',
      user_create: '',
      user_update: '',
    }

    this.db_history = Firebase.firestore.collection('medical_history');
    this.setDate = this.setDate.bind(this);
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  setDate (newDate) {
    this.setState({ date_entrance: newDate });
  }

  setLevel (level) {
    let tmpLevel;

    switch (level) {
      case 1:
        tmpLevel = [true, false, false, false];
        break;
      case 2:
        tmpLevel = [false, true, false, false];
        break;
      case 3:
        tmpLevel = [false, false, true, false];
        break;
      case 4:
        tmpLevel = [false, false, false, true];
        break;
      default:
        level = 1;
        break;
    }

    this.setState({
      level: tmpLevel,
      urgency: level
    })
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.setState({
        loading: false,
      })
    })
  }

  resetData = () => {
    this.setState({
      title: '',
      hospital: '',
      date_entrance: '',
      description: '',
      spend_extra: '',
      level: [
        true,
        false,
        false,
        false
      ],
      urgency: 1,
      family_name: '',
      active: false,
      prescription: {
        doctor_name: '',
        description: '',
        created_at: '',
        updated_at: '',
        user_create: '',
        user_update: ''
      },
      laboratories: [],
      spends: [],
    })
  }

  saveData = () => {
    if (this.state.title.trim() === '') {
      Alert.alert(
        'Campo faltante',
        'Algunos campos son obligatorios',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )

      return false
    }

    this.setState({
      loadingSave: true
    }, () => {
      let timestamp = new Date()

      this.db_history.add({
        title: this.state.title,
        date_entrance: this.state.date_entrance,
        hospital: this.state.hospital,
        description: this.state.description,
        level: this.state.urgency,
        family_name: this.state.family_name,
        spend_extra: this.state.spend_extra,
        active: true,
        user_patient: this.state.userLogged.uid,
        user_create: this.state.userLogged.uid,
        user_update: this.state.userLogged.uid,
        updated_at: timestamp,
        created_at: timestamp,
        laboratories: [],
        prescription: {
          doctor_name: '',
          description: '',
          created_at: timestamp,
          updated_at: timestamp,
          user_create: this.state.userLogged.uid,
          user_update: this.state.userLogged.uid
        },
      })
      .then(() => {
        this.resetData()
        this.props.navigation.navigate('History', {
          reloadList: true
        })
      })
      .catch((error) => {
        console.log(error)
        // this.setState({
        //   // errorSave: 'No se pudo crear el registro, intente nuevamente.',
        //   loadingSave: false,
        // })
      })
    })
  }

  renderButton () {
    if (this.state.loadingSave) {
      return (
        <ActivityIndicator size="large" />
      )
    }

    return (
      <Button onPress={() => this.saveData() }>Agregar</Button>
    )
  }

  renderInputFamiliar = () => {
    if (this.state.switchVal) {
      return (
        <View>
          <Input
            placeholder="Nombre del familiar"
            placeholderTextColor={'#ccc'}
            style={styles.inputForm}
            onChangeText={(family_name) => this.setState({family_name})}>
          {this.state.family_name}
          </Input>
        </View>
      )
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <Form style={{
              padding: 10,
            }}>
            <View>
              <Label style={styles.labelForm}>¿Cúal es el problema? *</Label>
              <Input style={styles.inputForm}
                placeholder="Ingresa la causa de la visita médica"
                placeholderTextColor={'#ccc'}
                onChangeText={title => this.setState({title})}>{this.state.title}</Input>
            </View>

            <View style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
            }}>
                <View style={{
                  flex: 1,
                  paddingTop: 10,
                  paddingBottom: 10,
                  paddingRight: 10,
                }}>
                  <Label style={{
                    color: '#00aeef',
                    fontSize: 14,
                    flex: 1,
                    width: '100%',
                  }}>Clínica/Hospital *</Label>
                  <Input style={styles.inputForm}
                    onChangeText={hospital => this.setState({hospital})}>{this.state.hospital}</Input>

                </View>
                <View style={{
                  flex: 1,
                  paddingTop: 10,
                  paddingBottom: 10,
                  paddingRight: 10,
                }}>
                  <Label style={styles.labelForm}>Fecha de ingreso *</Label>
                  <DatePicker
                    style={styles.inputForm}
                    defaultDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText="dd/mm/yyyy"
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                    onDateChange={this.setDate}
                  />
                </View>
            </View>

            <View>
              <Label style={styles.labelForm}>Síntomas *</Label>
              <Textarea bordered
                style={styles.textarea}
                onChangeText={description => this.setState({description})}
                rowSpan={3}>
              {this.state.description}
              </Textarea>
            </View>
            <View>
              <Label style={styles.labelForm}>Nivel de gravedad *</Label>
            </View>
            <View style={styles.listOptions}>
              <ListItem style={styles.optionList}>
                <Left>
                  <Ionicons name="md-square" size={18} style={styles.icons} color={'green'} />
                  <Text style={styles.optionMinus}>Consulta</Text>
                </Left>
                <Right>
                  <Radio selected={this.state.level[0]} onPress={() => this.setLevel(1)} color={'green'} />
                </Right>
              </ListItem>
              <ListItem style={styles.optionList}>
                <Left>
                  <Ionicons name="md-square" size={18} style={styles.icons} color={'yellow'} />
                  <Text style={styles.optionUrgency}>Urgencia</Text>
                </Left>
                <Right>
                  <Radio selected={this.state.level[1]} onPress={() => this.setLevel(2)} color={'yellow'}/>
                </Right>
              </ListItem>
              <ListItem style={styles.optionList}>
                <Left>
                  <Ionicons name="md-square" size={18} style={styles.icons} color={'orange'} />
                  <Text style={styles.optionEmergency}>Emergencia</Text>
                </Left>
                <Right>
                  <Radio selected={this.state.level[2]} onPress={() => this.setLevel(3)} color={'orange'} />
                </Right>
              </ListItem>
              <ListItem style={styles.optionList}>
                <Left>
                  <Ionicons name="md-square" size={18} style={styles.icons} color={'red'} />
                  <Text style={styles.optionSurgery}>Cirugía</Text>
                </Left>
                <Right>
                  <Radio selected={this.state.level[3]} onPress={() => this.setLevel(4)} color={'red'} />
                </Right>
              </ListItem>
            </View>

            <View>
              <Label style={styles.labelForm}>Gasto realizado *</Label>
              <Input style={styles.inputForm}
                placeholder="$0.00"
                placeholderTextColor={'#ccc'}
                onChangeText={spend_extra => this.setState({spend_extra})}>{this.state.spend_extra}</Input>
            </View>

            <View style={{
              marginTop: 10,
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
              <Label style={styles.labelForm}>¿Tiene un familiar acompañandolo?</Label>
              <Switch value={this.state.switchVal}
                onValueChange={(switchVal) => this.setState({switchVal})}/>
            </View>

            {this.renderInputFamiliar()}

            <View style={styles.footer}>
              {this.renderButton()}
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  hori: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  textarea: {
    // width: '100%',
    marginBottom: 10,
  },
  optionList: {
    padding: 0,
    flex: 1,
  },
  optionMinus: {
    fontSize: 14,
    color: 'green',
    padding: 0,
  },
  optionUrgency: {
    fontSize: 14,
    color: 'yellow',
    padding: 0,
  },
  optionEmergency: {
    fontSize: 14,
    color: 'orange',
    padding: 0,
  },
  optionSurgery: {
    fontSize: 14,
    color: 'red',
    padding: 0,
  },
  icons: {
    marginRight: 10,
  },
  labelForm: {
    color: '#00aeef',
    fontSize: 14,
    padding: 0,
    margin: 0,
  },
  listOptions: {
    marginTop: 10,
    marginBottom: 10,
  },
  inputForm: {
    color: '#333',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ccc',
    padding: 0,
    marginTop: 0,
    marginBottom: 10,
  },
  footer: {
    padding: 15,
  }
});
