import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  KeyboardAvoidingView,
  TouchableHighlight,
  ActivityIndicator,
  ListView,
  FlatList,
} from 'react-native'

import Firebase from '../../lib/firebase'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { Container, Content, Left, Item, Right, Icon, Text, Picker, Button, Fab, Toast, ActionSheet,
  Form, Label, Input, Card, CardItem, Body, List, ListItem, DatePicker, Textarea } from 'native-base'

import * as moment from 'moment-timezone'

var BUTTONS = [
  { text: "Nuevo diagnostico", icon: "clipboard", iconColor: "#2c8ef4" },
  { text: "Nuevo estudio médico", icon: "flask", iconColor: "#f42ced" },
  { text: "Nuevo gasto", icon: "cash", iconColor: "#ea943b" },
  { text: "Eliminar registro", icon: "trash", iconColor: "#fa213b" },
  { text: "Cerrar", icon: "close", iconColor: "#25de5b" }
]

var ADD_DIAG = 0
var ADD_LABS = 1
var ADD_SPEND = 2
var DELETE_INDEX = 3
var CANCEL_INDEX = 4

export default class HistoryDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // title: navigation.getParam('title', 'Detalle'),
      headerRight: (
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('edit')}>
            <Icon type="MaterialIcons" name="edit"></Icon>
          </TouchableHighlight>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('more')}>
            <Icon name="md-more"></Icon>
          </TouchableHighlight>
        </View>
      ),
    }
  }

  constructor (props) {
    super(props)
    const { navigation } = this.props
    const id = navigation.getParam('id', '')

    this.state = {
      id_detail: id,
      userLogged: null,
      loading: true,
      reloading: false,
      activeFab: false,
      editing: false,
      saving: false,
      deleting: false,

      title: '',
      date_entrance: '',
      hospital: '',
      description: '',
      level: 1,
      family_name: '',
      spend_extra: 0,
      active: false,
      user_patient: '',
      user_create: '',
      user_update: '',
      created_at: null,
      updated_at: null,

      listSpendData: [],
      listDiagsData: [],
      listLabsData: [],
    }

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.db_history = Firebase.firestore.collection('medical_history').doc(id)
    this.db_spends = Firebase.firestore.collection('medical_history').doc(id).collection('spends')
    this.db_diagnostics = Firebase.firestore.collection('medical_history').doc(id).collection('diagnostics')
    this.db_labs = Firebase.firestore.collection('medical_history').doc(id).collection('laboratories')
    this.setDate = this.setDate.bind(this)
  }

  _keyExtractor = (item, index) => index.toString()

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  _load = () => {
    this.db_history
    .get()
    .then((doc) => {
      if (doc.exists) {
        let tmpData = doc.data()

        let dateDefault = new Date(
          moment.unix(tmpData.date_entrance.seconds).format("YYYY/MM/DD")
        )
        let datetmp =
            dateDefault.getDate() + '-'+ (dateDefault.getMonth() + 1) + '-' + dateDefault.getFullYear()

        this.setState({
          title: tmpData.title,
          date_entrance: tmpData.date_entrance,
          dateShow: datetmp,
          hospital: tmpData.hospital,
          description: tmpData.description,
          level: tmpData.level,
          family_name: tmpData.family_name,
          spend_extra: tmpData.spend_extra,
          active: tmpData.active,
          user_patient: tmpData.user_patient,
          user_create: tmpData.user_create,
          user_update: tmpData.user_update,
          created_at: tmpData.created_at,
          updated_at: tmpData.updated_at,

          loading: false,
        })

      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch((error) => {
      console.log("Error getting document:", error);
    })

    this.db_spends
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length
      let dats = []

      if (count) {
        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD/MM/YYYY")

          let tmpData = {
            id: doc.id,
            price: doc.data().price,
            title: doc.data().title,
            updated_at: datetmp,
          }

          dats.push(tmpData)
        })
      }

      this.setState({
        listSpendData: [...this.state.listSpendData, ...dats],
      })
    })

    this.db_diagnostics
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length
      let diags = []

      if (count) {
        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD/MM/YYYY")

          let tmpData = {
            id: doc.id,
            doctor_name: doc.data().doctor_name,
            updated_at: datetmp,
          }

          diags.push(tmpData)
        })
      }

      this.setState({
        listDiagsData: [...this.state.listDiagsData, ...diags],
      })
    })

    this.db_labs
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length
      let labs = []

      if (count) {
        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format('DD/MM/YYYY')

          let tmpData = {
            id: doc.id,
            title: doc.data().title,
            updated_at: datetmp,
          }

          labs.push(tmpData)
        })
      }

      this.setState({
        listLabsData: [...this.state.listLabsData, ...labs],
      })
    })
  }

  _reload = () => {
    this.setState({
      loading: true,
      activeFab: false,

      title: '',
      date_entrance: '',
      hospital: '',
      description: '',
      level: 1,
      family_name: '',
      spend_extra: 0,
      active: false,
      user_patient: '',
      user_create: '',
      user_update: '',

      listDiagsData: [],
      listSpendData: [],
      listLabsData: [],
    }, () => {
      this._load()
    })
  }

  _onUpdate = () => {
    this.setState({
      saving: true
    }, () => {
      let timestamp = new Date()
      this.db_history.update({
        title: this.state.title,
        hospital: this.state.hospital,
        date_entrance: this.state.date_entrance,
        level: this.state.level,
        spend_extra: this.state.spend_extra,
        family_name: this.state.family_name,
        description: this.state.description,
        user_update: this.state.userLogged.uid,
        updated_at: timestamp
      })
      .then(() => {
        this.setState({
          saving: false,
          editing: false,
        })
      })
      .catch((error) => {
        console.error("Error updating document: ", error)
      })
    })
  }

  _delete = () => {
    this.setState({
      deleting: true,
    }, () => {
      let timestamp = new Date()

      this.db_history.update({
        updated_at: timestamp,
        active: false,
      })
      .then(() => {
        this.setState({
          deleting: false
        }, () => {
          Toast.show({
            text: 'Registro eliminado',
          })
          this.props.navigation.navigate('History', {
            reloadList: true
          })
        })
      })
      .catch((error) => {
        this.setState({deleting: false})
        Toast.show({
          text: 'No se pudo eliminar, intente nuevamente',
          buttonText: 'Okay'
        })
      })
    })
  }

  editData = () => {
    this.setState({
      editing: true
    })
  }

  setDate (newDate) {
    let datetmp = newDate.getDate() + '-'+ (newDate.getMonth() + 1) + '-' + newDate.getFullYear()

    this.setState({
      date: newDate,
      dateShow: datetmp,
    })
  }

  moreAction = () => {
    ActionSheet.show({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX,
      destructiveButtonIndex: DELETE_INDEX,
      // title: "Selecciona una opción"
    }, (buttonIndex) => {
      switch (buttonIndex) {
        case ADD_DIAG:
          this.props.navigation.push('DiagnosticAdd', {
            id: this.state.id_detail
          })
          break;
        case ADD_LABS:
          this.props.navigation.push('LabsAdd', {
            id: this.state.id_detail
          })
          break;
        case ADD_SPEND:
          this.props.navigation.push('SpendAdd', {
            id: this.state.id_detail
          })
          break;
        case DELETE_INDEX:
          Alert.alert(
            '¿Eliminar registro?',
            '¿Deseas borrar este registro?',
            [
              {text: 'Cancelar', onPress: () => {
              }, style: 'cancel'},
              {text: 'Sí, eliminar', onPress: () => {
                this._delete()
              }},
            ],
            { cancelable: false }
          )
          break;
        default:
          break;
      }
    })
  }

  onChangeLevel(value: string) {
    this.setState({
      level: value
    })
  }

  renderLevel = () => {
    if (!this.state.editing) {
      switch (this.state.level) {
        case '1':
          return <Text style={{ paddingTop: 13, paddingBottom: 10, color: 'green' }}>I - Consulta</Text>
        case '2':
          return <Text style={{ paddingTop: 13, paddingBottom: 10, color: 'yellow' }}>II - Urgencia</Text>
        case '3':
          return <Text style={{ paddingTop: 13, paddingBottom: 10, color: 'orange' }}>III - Emergencia</Text>
        case '4':
          return <Text style={{ paddingTop: 13, paddingBottom: 10, color: 'red' }}>IV - Cirugía</Text>
        default:
          return <Text style={{ paddingTop: 13, paddingBottom: 10 }}>No establecido</Text>
      }
    } else {
      return (
        <Picker
          mode="dropdown"
          iosIcon={<Icon name="ios-arrow-down-outline" />}
          style={{ width: undefined }}
          placeholder="Select your SIM"
          placeholderStyle={{ color: "#bfc6ea" }}
          placeholderIconColor="#007aff"
          selectedValue={this.state.level}
          onValueChange={this.onChangeLevel.bind(this)}
        >
          <Picker.Item label="I - Consulta" value="1" />
          <Picker.Item label="II - Urgencia" value="2" />
          <Picker.Item label="III - Emergencia" value="3" />
          <Picker.Item label="IV - Cirugía" value="4" />
        </Picker>
      )
    }
  }

  renderSpendList = () => {
    if (this.state.listSpendData.length === 0) {
      return false
    }

    return (
      <View>
        <Text style={styles.subtitleList}>Gastos extras</Text>
        <FlatList
          data={this.state.listSpendData}
          keyExtractor={this._keyExtractor}
          renderItem={(data) =>
            <ListItem onPress={() => {
              this.props.navigation.push('SpendDetail', {
                id_spend: data.item.id,
                id_history: this.state.id_detail,
              })
            }}>
              <Body>
                <Text>{data.item.title}</Text>
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-end',
                }}>
                  <Text style={{ color: 'blue', fontSize: 14 }}>$ {data.item.price}</Text>
                  <Text style={{ color: '#333', fontSize: 12 }}>
                    {data.item.updated_at}
                  </Text>
                </View>
              </Body>
              <Right>
                <Icon type="MaterialIcons" name="arrow-forward" />
              </Right>
            </ListItem>
          }
        />
      </View>
    )
  }

  renderDiagsList = () => {
    if (this.state.listDiagsData.length === 0) {
      return false
    }

    return (
      <View>
        <Text style={{
          fontSize: 16,
          margin: 10,
          color: 'green',
        }}>Diagnosticos médicos</Text>
        <FlatList
          data={this.state.listDiagsData}
          keyExtractor={this._keyExtractor}
          renderItem={(data) =>
            <ListItem onPress={() => {
              this.props.navigation.push('DiagnosticDetail', {
                id_diags: data.item.id,
                id_history: this.state.id_detail,
              })
            }}>
              <Body>
                <Text>{data.item.doctor_name}</Text>
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-end',
                }}>
                  <Text style={{ color: '#333', fontSize: 12 }}>
                    {data.item.updated_at}
                  </Text>
                </View>
              </Body>
              <Right>
                <Icon type="MaterialIcons" name="arrow-forward" />
              </Right>
            </ListItem>
          }
        />
      </View>
    )
  }

  renderLabsList = () => {
    if (this.state.listLabsData.length === 0) {
      return false
    }

    return (
      <View>
        <Text style={styles.subtitleList}>Análisis de laboratorios</Text>
        <FlatList
          data={this.state.listLabsData}
          keyExtractor={this._keyExtractor}
          renderItem={(data) =>
            <ListItem onPress={() => {
              this.props.navigation.push('LabsDetail', {
                id: data.item.id,
                id_history: this.state.id_detail,
              })
            }}>
              <Body>
                <Text>{data.item.title}</Text>
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-end',
                }}>
                  <Text style={{ color: '#333', fontSize: 12 }}>
                    {data.item.updated_at}
                  </Text>
                </View>
              </Body>
              <Right>
                <Icon type="MaterialIcons" name="arrow-forward" />
              </Right>
            </ListItem>
          }
        />
      </View>
    )
  }

  renderDate = () => {
    if (!this.state.editing) {
      return <Input editable={false} maxLength={50}>{this.state.dateShow}</Input>
    }

    return (
      <DatePicker
        defaultDate={new Date(this.state.date_entrance)}
        locale={"en"}
        timeZoneOffsetInMinutes={undefined}
        modalTransparent={false}
        animationType={"fade"}
        androidMode={"default"}
        placeHolderText={this.state.dateShow}
        textStyle={{ color: "green", textAlign: 'left' }}
        placeHolderTextStyle={{ color: "#d3d3d3", textAlign: 'left' }}
        onDateChange={this.setDate}
      />
    )
  }

  renderFAB = () => {
    if (this.state.editing) {
      return (
        <Fab
          containerStyle={{ }}
          style={{ backgroundColor: '#34A34F' }}
          position="bottomRight"
          onPress={() => {
            this._onUpdate()
          }}>
          <Icon type="MaterialIcons" name="done" />
        </Fab>
      )
    }
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <Container style={styles.container}>
        <Content>
          <View style={styles.content}>
            <View>
              <Text style={styles.titleNote}>Motivo de la asistencia médica</Text>
              <Input editable={this.state.editing} maxLength = {50}
                onChangeText={title => this.setState({title})}>{this.state.title}</Input>
            </View>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <View style={{ flex: 1 }}>
                <Text style={styles.titleNote}>Clínica/Hospital</Text>
                <Input editable={this.state.editing} maxLength = {50}
                  onChangeText={hospital => this.setState({hospital})}>{this.state.hospital}</Input>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.titleNote}>Fecha de ingreso</Text>
                {this.renderDate()}
              </View>
            </View>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
            }}>
              <View style={{flex: 1}}>
                <Text style={styles.titleNote}>Nivel de gravedad</Text>
                {this.renderLevel()}
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.titleNote}>Gasto</Text>
                <Input editable={this.state.editing} maxLength={10} placeholder="$ 0.00"
                  onChangeText={spend_extra => this.setState({spend_extra})}>{this.state.spend_extra}</Input>
              </View>
            </View>
            <View>
              <Text style={styles.titleNote}>Nombre del familiar o persona presente</Text>
              <Input editable={this.state.editing} maxLength={50}
                onChangeText={family_name => this.setState({family_name})}>{this.state.family_name}</Input>
              <Text style={styles.titleNote}>Síntomas que presenta el paciente</Text>
              <Textarea bordered
                style={styles.textarea}
                editable={this.state.editing}
                onChangeText={description => this.setState({description})}
                rowSpan={3} value={this.state.description} />
            </View>
          </View>
          {
            // Renderizar lista diagnosticos
            this.renderDiagsList()
          }
          {
            // Renderizar lista laboratorios
            this.renderLabsList()
          }
          {
            // Renderizar lista de gastos extras
            this.renderSpendList()
          }
        </Content>
        {this.renderFAB()}
      </Container>
    )
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this._load()
    })

    this.props.navigation.setParams({ edit: this.editData })
    this.props.navigation.setParams({ more: this.moreAction })
  }

  componentDidUpdate (prevProps, prevState) {
    const { navigation } = this.props
    const reloadList = navigation.getParam('reloadList', false)

    // true AND false AND true
    if (reloadList && !this.state.reloading && !prevState.reloading) {
      this.setState({
        reloading: true,
      }, () => {
        this._reload()
      })
    }
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  content: {
    padding: 10,
    // backgroundColor: '#f2f2f2',
    // flexDirection: 'column',
  },
  pressContent: {
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  titleNote: {
    color: '#848484',
    fontSize: 13,
    marginTop: 8,
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F00'
  },
  subtitleList: {
    color: '#848484',
    fontSize: 16,
    margin: 10,
    color: 'green',
  }
})
