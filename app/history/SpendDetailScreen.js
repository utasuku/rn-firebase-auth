import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native'

import Firebase from '../../lib/firebase'
import * as moment from 'moment-timezone'

import { Container, Content, Left, Item, Right, Icon, Text, Picker, Button, Fab, ActionSheet, Thumbnail,
  Form, Label, Input, Card, CardItem, Body, List, ListItem, DatePicker, Textarea, H1, H2, H3 } from 'native-base'

var BUTTONS = ['Eliminar','Cerrar']
var DELETE_INDEX = 0
var CANCEL_INDEX = 1

const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

export default class SpendDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Gasto extra',
      headerRight: (
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('edit')}>
            <Icon type="MaterialIcons" name="edit"></Icon>
          </TouchableHighlight>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('more')}>
            <Icon type="MaterialIcons" name="more-vert"></Icon>
          </TouchableHighlight>
        </View>
      ),
    }
  }

  constructor (props) {
    super(props)
    const { navigation } = this.props;
    const idHistory = navigation.getParam('id_history', '')
    const idSpend = navigation.getParam('id_spend', '')

    this.state = {
      loading: true,
      userLogged: null,
      editing: false,
      activeFab: false,
      saving: false,
      deleting: false,

      title: '',
      price: '',
      description: '',
      date: new Date(),
      dateShow: '',
    }

    this.db_spend = Firebase.firestore.collection('medical_history').doc(idHistory).collection('spends').doc(idSpend)
    this.setDate = this.setDate.bind(this)
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  _getData = () => {
    this.db_spend
    .get()
    .then((doc) => {
      if (doc.exists) {
        let spnd = doc.data()

        let dateDefault = new Date(
          moment.unix(spnd.date.seconds).format("YYYY/MM/DD")
        )
        let datetmp =
            dateDefault.getDate() + '-'+ (dateDefault.getMonth() + 1) + '-' + dateDefault.getFullYear()

        this.setState({
          title: spnd.title,
          price: spnd.price,
          description: spnd.description,
          date: spnd.date,
          dateShow: datetmp,
          updated_at: spnd.updated_at,

          loading: false,
        })
      } else {
        console.log("No such document!")
      }
    }).catch((error) => {
      console.log("Error getting document:", error)
    })
  }

  _onUpdate = () => {
    this.setState({
      saving: true
    }, () => {
      let timestamp = new Date()

      this.db_spend.update({
        title: this.state.title,
        price: this.state.price,
        date: this.state.date,
        description: this.state.description,
        user_update: this.state.userLogged.uid,
        updated_at: timestamp
      })
      .then(() => {
        this.setState({
          saving: false,
          editing: false,
        })
      })
      .catch((error) => {
        console.error("Error updating document: ", error)
      })
    })
  }

  _delete = () => {
    this.setState({
      deleting: true,
    }, () => {
      let timestamp = new Date()

      this.db_spend.update({
        updated_at: timestamp,
        active: false,
      })
      .then(() => {
        this.setState({
          deleting: false
        }, () => {
          this.props.navigation.navigate('HistoryDetail', {
            reloadList: true
          })
        })
      })
      .catch((error) => {
        this.setState({deleting: false})
        Toast.show({
          text: 'No se pudo eliminar, intente nuevamente',
          buttonText: 'Okay'
        })
      })
    })
  }

  setDate (newDate) {
    let datetmp = newDate.getDate() + '-'+ (newDate.getMonth() + 1) + '-' + newDate.getFullYear()

    this.setState({
      date: newDate,
      dateShow: datetmp,
    })
  }

  editData = () => {
    this.setState({
      editing: true
    })
  }

  moreAction = () => {
    ActionSheet.show({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX,
      destructiveButtonIndex: DELETE_INDEX,
      title: "Selecciona una opción"
    }, (buttonIndex) => {
      if (buttonIndex === 0) {
        Alert.alert(
          '¿Eliminar registro?',
          '¿Deseas borrar este registro?',
          [
            {text: 'Cancelar', onPress: () => {
              console.log('Cancel Pressed');
            }, style: 'cancel'},
            {text: 'Sí, eliminar', onPress: () => {
              this._delete()
            }},
          ],
          { cancelable: false }
        )
      }
    })
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this._getData()
    })

    this.props.navigation.setParams({ edit: this.editData })
    this.props.navigation.setParams({ more: this.moreAction })
  }

  renderLoading = () => {
    if (!this.state.saving) {
      return false
    }

    return (
      <View style={{
        marginTop: 15,
        marginBottom: 15,
      }}>
        <ActivityIndicator size="large" />
      </View>
    )
  }

  renderFAB = () => {
    if (!this.state.editing) {
      return false
    }

    if (this.state.saving) {
      return (
        <View style={{
          marginTop: 15,
          marginBottom: 15,
        }}>
          <ActivityIndicator size="large" />
        </View>
      )
    } else {
      return (
        <Fab
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            this._onUpdate()
          }}>
          <Icon type="MaterialIcons" name="done" />
        </Fab>
      )
    }

  }

  renderDate = () => {
    if (!this.state.editing) {
      return <Input editable={false} maxLength={50}>{this.state.dateShow}</Input>
    }

    return (
      <DatePicker
        defaultDate={new Date(this.state.date)}
        locale={"en"}
        timeZoneOffsetInMinutes={undefined}
        modalTransparent={false}
        animationType={"fade"}
        androidMode={"default"}
        placeHolderText={this.state.dateShow}
        textStyle={{ color: "green" }}
        placeHolderTextStyle={{ color: "#d3d3d3" }}
        onDateChange={this.setDate}
      />
    )
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <Form>
            <Item stackedLabel>
              <Label>Motivo del gasto</Label>
              <Input editable={this.state.editing} maxLength = {50}
                onChangeText={title => this.setState({title})}>{this.state.title}</Input>
            </Item>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <View style={{
                flex: 1,
              }}>
                <Label>Precio/Costo</Label>
                <Input editable={this.state.editing} maxLength = {5}
                  onChangeText={price => this.setState({price})}
                  placeholder="$ 0.00" >{this.state.price}</Input>
              </View>
              <View style={{
                flex: 1,
              }}>
                <Label>Fecha</Label>
                {this.renderDate()}
              </View>
            </View>
            <Item stackedLabel last>
              <Label>Descripción</Label>
              <Textarea style={{width: '100%', marginTop: 10, marginBottom: 10}}
                onChangeText={description => this.setState({description})}
                editable={this.state.editing} rowSpan={7} bordered placeholder="Textarea" value={this.state.description} />
            </Item>
          </Form>
          <View style={{padding: 10}}>
            <H3>Galería</H3>
            <View>
              <Thumbnail square large source={{uri: uri}} />
              <Thumbnail square large source={{uri: uri}} />
              <Thumbnail square large source={{uri: uri}} />
            </View>
          </View>
        </Content>
        {this.renderFAB()}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F00'
  },
})
