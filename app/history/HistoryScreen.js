import React, { Component } from "react";
import {
  ActivityIndicator,
  View,
  StyleSheet,
  AsyncStorage,
  BackHandler,
  FlatList,
  ListView
} from "react-native";

import Ionicons from 'react-native-vector-icons/Ionicons';
import Firebase from '../../lib/firebase';

import { List, ListItem, Left, Body, Right, Icon, Fab, Text } from 'native-base';
import { Button } from '../../components/Button'
import * as moment from 'moment-timezone';

var EXP_LIMIT = 15;

export default class HistoryScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Historial clínico',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      userLogged: false,
      loading: true,
      reloading: false,
      loadingMore: false,
      loadingStart: true,
      activeFab: false,
      clicked: false,
      listViewData: [],
      isZero: false,
      refreshing: false,
      startAfter: '',
    }

    this.db_history = Firebase.firestore.collection("medical_history");
  }

  _keyExtractor = (item, index) => item.id;

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  _load = async () => {
    this.db_history
    .where('user_patient', '==', this.state.userLogged.uid)
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .startAfter(this.state.startAfter)
    .limit(EXP_LIMIT)
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length;
      let dats = []

      if (count) {
        // var lastVisible = snapshot.docs[count - 1];
        // this.setState({startAfter: lastVisible})

        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD-MM-YYYY HH:mm")

          let tmpData = {
            id: doc.id,
            title: doc.data().title,
            updated_at: datetmp,
          }

          dats.push(tmpData)
        })
      }

      this.setState(state => ({
        listViewData: [...this.state.listViewData, ...dats],
        loadingMore: false,
        modalVisible: false,
        isZero: (count < EXP_LIMIT) ? true: false,
      }))
    })
  }

  _reload = () => {
    this.db_history
    .where('user_patient', '==', this.state.userLogged.uid)
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .startAfter(this.state.startAfter)
    .limit(EXP_LIMIT)
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length;
      let dats = []

      if (count) {
        // var lastVisible = snapshot.docs[count - 1];
        // this.setState({startAfter: lastVisible})

        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD-MM-YYYY HH:mm")

          let tmpData = {
            id: doc.id,
            title: doc.data().title,
            updated_at: datetmp,
          }

          dats.push(tmpData)
        })
      }

      this.setState(state => ({
        listViewData: dats,
        loading: false,
        refreshing: false,
        loadingMore: false,
        isZero: (count < EXP_LIMIT) ? true: false,
      }))
    })
  }

  onPressDetail (item) {
    if (this.state.clicked) {
      return false
    }

    this.setState({
      clicked: true
    }, () => {
      this.props.navigation.navigate('HistoryDetail', {
        id: item.id,
        title: item.title,
      })

      setTimeout(() => {
        this.setState({clicked: false})
      }, 1000);
    })
  }

  onRefresh = () => {
    if (this.state.refreshing) {
      return false
    }

    this.setState({
      refreshing: true,
      startAfter: ''
    }, () => {
      this._reload()
    })
  }

  onEndReached = () => {
    if (this.state.loadingMore || this.state.isZero || this.state.loadingStart) {
      return false
    }

    this.setState({
      loadingMore: true,
    },
    () => {
      this._load()
    })
  }


  componentWillMount () {
    this._retrieveData().then(() => {
      this._load().then(() => {
        setTimeout(() => {
          this.setState({
            loading: false,
            loadingStart: false,
          })
        }, 1500)
      });
    })
  }

  componentDidUpdate (prevProps, prevState) {
    const { navigation } = this.props;
    const reloadList = navigation.getParam('reloadList', false);

    if (reloadList && !this.state.reloading && !prevState.reloading) {
      this.setState({
        listViewData: [],
        reloading: true,
        startAfter: ''
      }, () => {
        this._reload();
      })
    }
  }

  render() {
    if (this.state.loading || this.state.loadingStart) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <View style={styles.container}>

        <FlatList
          data={this.state.listViewData}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => (
            <ListItem onPress={() => {
              this.onPressDetail(item)
            }}>
              <Body>
                <Text>{item.title}</Text>
                <Text note style={styles.italic}>Última actualización {item.updated_at}.</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          )}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          // ListFooterComponent={() => this.renderListFooter()}
          ListFooterComponent={() => {
              if (this.state.loadingMore) {
                return <ActivityIndicator style={styles.loadingMore} animating />
              } else {
                return null
              }
            }
          }
          onEndReached={() => this.onEndReached()}
          onEndReachedThreshold={0.5}
        />

        <Fab
          active={this.state.activeFab}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            this.props.navigation.navigate('HistoryAdd')
          }}>
          <Icon name="add" />
        </Fab>
      </View>
    );
  }

  componentDidMount () {

  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  italic: {
    fontStyle: 'italic'
  },
});
