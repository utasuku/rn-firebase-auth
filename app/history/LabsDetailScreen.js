import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  ActivityIndicator,
  TouchableHighlight
} from "react-native";

import Firebase from '../../lib/firebase';
import * as moment from 'moment-timezone'

import { Container, Content, Left, Item, Right, Icon, Text, Picker, Button, Fab, Toast, ActionSheet,
  Form, Label, Input, Card, CardItem, Body, List, ListItem, DatePicker, Textarea } from 'native-base'

var BUTTONS = ['Eliminar','Cerrar']
var DELETE_INDEX = 0
var CANCEL_INDEX = 1

export default class LabsDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Estudio clínico',
      headerRight: (
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('edit')}>
            <Icon type="MaterialIcons" name="edit"></Icon>
          </TouchableHighlight>
          <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('more')}>
            <Icon type="MaterialIcons" name="more-vert"></Icon>
          </TouchableHighlight>
        </View>
      ),
    }
  }

  constructor (props) {
    super(props)

    const { navigation } = this.props;
    const idHistory = navigation.getParam('id_history', '');
    const idLab = navigation.getParam('id', '');

    this.state = {
      loading: true,
      userLogged: null,
      editing: false,
      activeFab: false,
      saving: false,
      deleting: false,

      title: '',
      price: '',
      description: '',
      date: null,
      dateShow: null,
    }

    this.db_lab = Firebase.firestore.collection('medical_history')
      .doc(idHistory).collection('laboratories').doc(idLab)
    this.setDate = this.setDate.bind(this)
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser')
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
    } catch (error) {
    // Error retrieving data
    }
  }

  _getData = () => {
    this.db_lab
    .get()
    .then((doc) => {
      if (doc.exists) {
        let data = doc.data()

        let dateDefault = new Date(
          moment.unix(data.date.seconds).format("YYYY/MM/DD")
        )
        let datetmp =
            dateDefault.getDate() + '-'+ (dateDefault.getMonth() + 1) + '-' + dateDefault.getFullYear()

        this.setState({
          title: data.title,
          description: data.description,
          price: data.price,
          date: data.date,
          dateShow: datetmp,
          updated_at: data.updated_at,

          loading: false,
        })
      } else {
        console.log("No such document!")
      }
    }).catch((error) => {
      console.log("Error getting document:", error)
    })
  }

  _onUpdate = () => {
    this.setState({
      saving: true
    }, () => {
      let timestamp = new Date()
      this.db_lab.update({
        title: this.state.title,
        price: this.state.price,
        description: this.state.description,
        date: this.state.date,
        user_update: this.state.userLogged.uid,
        updated_at: timestamp
      })
      .then(() => {
        this.setState({
          saving: false,
          editing: false,
        }, () => {
          Toast.show({
            text: "Document successfully updated!",
            duration: 3000
          })
        })
      })
      .catch((error) => {
        console.error("Error updating document: ", error)
      })
    })
  }

  editData = () => {
    this.setState({
      editing: true
    })
  }

  _delete = () => {
    this.setState({
      deleting: true,
    }, () => {
      let timestamp = new Date()

      this.db_lab.update({
        updated_at: timestamp,
        active: false,
      })
      .then(() => {
        this.setState({
          deleting: false
        }, () => {
          Toast.show({
            text: 'Registro eliminado',
          })
          this.props.navigation.navigate('HistoryDetail', {
            reloadList: true
          })
        })
      })
      .catch((error) => {
        this.setState({deleting: false})
        Toast.show({
          text: 'No se pudo eliminar, intente nuevamente',
          buttonText: 'Okay'
        })
      })
    })
  }

  setDate (newDate) {
    let datetmp = newDate.getDate() + '-'+ (newDate.getMonth() + 1) + '-' + newDate.getFullYear()

    this.setState({
      date: newDate,
      dateShow: datetmp,
    })
  }

  moreAction = () => {
    ActionSheet.show({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX,
      destructiveButtonIndex: DELETE_INDEX,
      title: "Selecciona una opción"
    }, (buttonIndex) => {
      if (buttonIndex === 0) {
        Alert.alert(
          '¿Eliminar registro?',
          '¿Deseas borrar este registro?',
          [
            {text: 'Cancelar', onPress: () => {
            }, style: 'cancel'},
            {text: 'Sí, eliminar', onPress: () => {
              this._delete()
            }},
          ],
          { cancelable: false }
        )
      }
    })
  }

  componentDidMount() {
    this._retrieveData().then(() => {
      this._getData()
    })

    this.props.navigation.setParams({ edit: this.editData })
    this.props.navigation.setParams({ more: this.moreAction })
  }

  renderFAB = () => {
    if (!this.state.editing) {
      return false
    }

    if (this.state.saving) {
      return (
        <View style={{
          marginTop: 15,
          marginBottom: 15,
        }}>
          <ActivityIndicator size="large" />
        </View>
      )
    } else {
      return (
        <Fab
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            this._onUpdate()
          }}>
          <Icon type="MaterialIcons" name="done" />
        </Fab>
      )
    }
  }

  renderDate = () => {
    if (!this.state.editing) {
      return <Input editable={false} maxLength={50}>{this.state.dateShow}</Input>
    }

    return (
      <DatePicker
        defaultDate={new Date(this.state.date)}
        locale={"en"}
        timeZoneOffsetInMinutes={undefined}
        modalTransparent={false}
        animationType={"fade"}
        androidMode={"default"}
        placeHolderText={this.state.dateShow}
        textStyle={{ color: "green", textAlign: 'left' }}
        placeHolderTextStyle={{ color: "#d3d3d3", textAlign: 'left' }}
        onDateChange={this.setDate}
      />
    )
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <Form>
            <Item stackedLabel>
              <Label>Estudio de laboratorio:</Label>
              <Input editable={this.state.editing} maxLength = {50}
                onChangeText={title => this.setState({title})}>{this.state.title}</Input>
            </Item>
            <Item stackedLabel>
              <Label>Precio:</Label>
              <Input editable={this.state.editing} maxLength = {5}
                onChangeText={price => this.setState({price})}>{this.state.price}</Input>
            </Item>
            <Item>
              <Label>Fecha: </Label>
              {this.renderDate()}
            </Item>
            <Item stackedLabel last>
              <Label>Descripción:</Label>
              <Textarea style={{width: '100%', marginTop: 10, marginBottom: 10}}
                onChangeText={description => this.setState({description})}
                editable={this.state.editing} bordered rowSpan={7}  value={this.state.description} />
            </Item>
          </Form>
        </Content>
        {this.renderFAB()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F00'
  },
})
