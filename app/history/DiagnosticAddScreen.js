import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";

import Firebase from '../../lib/firebase'

import { Container, Content, Item, Right, Icon, Text, Fab, Toast,
  Form, Label, Input, DatePicker, Textarea } from 'native-base'

export default class DiagnosticAddScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Nueva consulta',
    }
  }

  constructor (props) {
    super(props)

    const { navigation } = this.props
    const id = navigation.getParam('id', '')

    this.state = {
      loading: true,
      userLogged: null,
      saving: false,

      doctor_name: '',
      date: null,
      price: null,
      description: '',
    }

    this.db_diags = Firebase.firestore.collection('medical_history').doc(id).collection('diagnostics')
    this.setDate = this.setDate.bind(this)
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser')
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
    } catch (error) {
    // Error retrieving data
    }
  }

  _getData = () => {

  }

  _onCreate = () => {
    if (this.state.doctor_name.trim() === '' ||
      this.state.price.trim() === '' ||
      this.state.description.trim() === '' ||
      this.state.date === null) {
      return false
    }

    this.setState({
      saving: true,
    }, () => {
      let timestamp = new Date()

      this.db_diags.add({
        doctor_name: this.state.doctor_name,
        price: this.state.price,
        description: this.state.description,
        date: this.state.date,
        user_create: this.state.userLogged.uid,
        user_update: this.state.userLogged.uid,
        created_at: timestamp,
        updated_at: timestamp,
        active: true,
      })
      .then(() => {
        Toast.show({
          text: "Document added succesfully!",
          duration: 3000
        })
        this.props.navigation.navigate('HistoryDetail', {
          id: this.state.id_history,
          reloadList: true,
        })
      })
      .catch((error) => {
        console.log('error', error)
      })
    })
  }

  setDate (newDate) {
    this.setState({
      date: newDate,
    })
  }

  renderFab = () => {
    if (this.state.saving) {
      return (
        <ActivityIndicator size="large" />
      )
    }

    return (
      <Fab
        containerStyle={{ }}
        style={{ backgroundColor: '#5067FF' }}
        position="bottomRight"
        onPress={() => {
          this._onCreate()
        }}>
        <Icon type="MaterialIcons" name="done" />
      </Fab>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <Form>
            <Item stackedLabel>
              <Label>Doctor(a):</Label>
              <Input maxLength={50}
                onChangeText={doctor_name => this.setState({doctor_name})}>{this.state.doctor_name}</Input>
            </Item>
            <Item stackedLabel>
              <Label>Precio:</Label>
              <Input maxLength={50} placeholder="$ 0.00"
                onChangeText={price => this.setState({price})}>{this.state.price}</Input>
            </Item>
            <Item>
              <Label>Fecha de diagnostico: </Label>
              <DatePicker
                defaultDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={'Selecciona la fecha...'}
                textStyle={{ color: "green", textAlign: 'left' }}
                placeHolderTextStyle={{ color: "#d3d3d3", textAlign: 'left' }}
                onDateChange={this.setDate}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Descripción:</Label>
              <Textarea style={{width: '100%', marginTop: 10, marginBottom: 10}}
                onChangeText={description => this.setState({description})}
                bordered rowSpan={7}  value={this.state.description} />
            </Item>
          </Form>
        </Content>
        {this.renderFab()}
      </Container>
    )
  }

  componentDidMount() {
    this._retrieveData().then(() => {
      this.setState({loading: false})
    })
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F00'
  },
})

