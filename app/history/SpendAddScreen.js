import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  ActivityIndicator,
} from 'react-native'

import Firebase from '../../lib/firebase'
import { Container, Content, Left, Item, Right, Icon, Text, Picker, Button, Fab, Toast,
  Form, Label, Input, Card, CardItem, Body, List, ListItem, DatePicker, Textarea } from 'native-base'

export default class SpendAddScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Nuevo gasto',
    }
  }

  constructor (props) {
    super(props)

    const { navigation } = this.props
    const id = navigation.getParam('id', '')

    this.state = {
      id_history: id,
      loading: true,
      userLogged: null,
      saving: false,

      title: '',
      price: '',
      description: '',
      date: null,
    }

    this.db_spends = Firebase.firestore.collection('medical_history').doc(id).collection('spends')
    this.setDate = this.setDate.bind(this)
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  _add = () => {
    if (this.state.title.trim() === '' ||
      this.state.price.trim() === '' ||
      this.state.description.trim() === '' ||
      this.state.date === null) {
      return false
    }

    this.setState({
      saving: true,
    }, () => {
      let timestamp = new Date()
      this.db_spends.add({
        title: this.state.title,
        price: this.state.price,
        description: this.state.description,
        date: this.state.date,
        user_create: this.state.userLogged.uid,
        user_update: this.state.userLogged.uid,
        created_at: timestamp,
        updated_at: timestamp,
        active: true,
      })
      .then(() => {
        Toast.show({
          text: "Document added succesfully!",
          duration: 3000
        })
        this.props.navigation.navigate('HistoryDetail', {
          id: this.state.id_history,
          reloadList: true,
        })
      })
      .catch((error) => {
        console.log('error', error)
      })
    })
  }

  setDate (newDate) {
    this.setState({ date: newDate });
  }

  renderFab = () => {
    if (this.state.saving) {
      return (
        <ActivityIndicator size="large" />
      )
    }

    return (
      <Fab
        containerStyle={{ }}
        style={{ backgroundColor: '#5067FF' }}
        position="bottomRight"
        onPress={() => {
          this._add()
        }}>
        <Icon type="MaterialIcons" name="done" />
      </Fab>
    )
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }


    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <Form>
            <Item stackedLabel>
              <Label>Motivo del gasto</Label>
              <Input onChangeText={title => this.setState({title})}
                placeholder="Motivo del estudio" />
            </Item>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <View style={{
                flex: 1,
              }}>
                <Label>Precio/Costo</Label>
                <Input onChangeText={price => this.setState({price})}
                  placeholder="$ 0.00" />
              </View>
              <View style={{
                flex: 1,
              }}>
                <Label>Fecha</Label>
                <DatePicker
                  defaultDate={new Date()}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText="dd/mm/yyyy"
                  textStyle={{ color: "green" }}
                  placeHolderTextStyle={{ color: "#d3d3d3" }}
                  onDateChange={this.setDate}
                />
              </View>
            </View>
            <Item stackedLabel last>
              <Label>Descripción</Label>
              <Textarea style={{width: '100%', marginTop: 10, marginBottom: 10}}
                onChangeText={description => this.setState({description})}
                rowSpan={7} bordered placeholder="Textarea" />
            </Item>
          </Form>
        </Content>
        {this.renderFab()}
      </Container>
    )
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.setState({loading: false})
    })
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#F00'
  }
})
