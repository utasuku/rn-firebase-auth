import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  TouchableHighlight,
} from "react-native";

import Firebase from '../../lib/firebase'
import { Container, Content, Icon, Text, Fab, Header, Left, Right, Body, Thumbnail,
  Card, CardItem, Badge, Tab, Tabs } from 'native-base'

export default class ProfileViewScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor (props) {
    super(props)
  }

  _retrieveData = () => {

  }

  _getData = () => {

  }

  render() {
    const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

    return (
      <Container>
        <View style={{
          backgroundColor: 'blue',
          height: 200,
        }}>
          <View style={{
            position: 'absolute',
            zIndex: 100,
            left: 25,
            top: 35,
            opacity: 1,
          }}>
            <TouchableHighlight>
              <Icon name="arrow-back" style={{color: 'white'}} />
            </TouchableHighlight>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column'
          }}>
            <View style={{
                backgroundColor: 'white',
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
            }}>
              <Thumbnail source={require('../../assets/user.png')} />
            </View>
            <Text style={{ color: 'white', marginTop: 5, fontSize: 16 }}>Ulises García Salinas</Text>
            <Text style={{ color: 'white', marginTop: 0, fontSize: 12 }}>Estado: Enfermo</Text>
          </View>
        </View>
        <Content>
          <Card>
            <CardItem header bordered>
              <Text>Alergías</Text>
            </CardItem>
            <CardItem bordered>
              <View style={{ flexDirection: 'row' }}>
                <Badge style={{ marginLeft: 5 }}><Text>Arena</Text></Badge>
                <Badge style={{ marginLeft: 5 }}><Text>Penicilina</Text></Badge>
                <Badge style={{ marginLeft: 5 }}><Text>Animales</Text></Badge>
              </View>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Registros médicos</Text>
            </CardItem>
            <CardItem style={{ flexDirection: 'row', padding: 10, justifyContent: 'flex-start' }}>
              <View style={{ flexDirection: 'column', paddingLeft: 5, paddingRight: 5, alignItems: 'center' }}>
                <Text style={{ fontSize: 14, color: 'gray' }}>Julio</Text>
                <Text style={{ fontSize: 32, color: 'green', fontWeight: 'bold' }}>12</Text>
                <Text style={{ fontSize: 14, color: 'gray' }}>2018</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'column', padding: 10, backgroundColor: '#f2f2f2' }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 14, color: 'gray' }}>Hora</Text>
                    <Text style={{ fontSize: 16, color: '#333' }}>17:00</Text>
                  </View>
                  <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5 }}>
                    <Text style={{ fontSize: 14, color: 'gray' }}>Clínica/Hospital</Text>
                    <Text style={{ fontSize: 16, color: '#333' }}>IMSS 45</Text>
                  </View>
                </View>
                <View>
                  <Text style={{ fontSize: 14, color: 'gray' }}>Motivo de asistencia médica</Text>
                  <Text style={{ fontSize: 16, color: '#333' }}>Brazo roto</Text>
                </View>
              </View>
            </CardItem>
          </Card>
          <Card>
            <CardItem header bordered>
              <Text>Amigos en común</Text>
            </CardItem>
            <CardItem style={{ flexDirection: 'row' }}>
              <Thumbnail style={{ marginLeft: 10 }} small source={{uri: uri}} />
              <Thumbnail style={{ marginLeft: 10 }} small source={{uri: uri}} />
              <Thumbnail style={{ marginLeft: 10 }} small source={{uri: uri}} />
              <Thumbnail style={{ marginLeft: 10 }} small source={{uri: uri}} />
            </CardItem>
          </Card>
        </Content>
      </Container>
    )
  }

  componentDidMount() {

  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#FFF',
  },
  header: {
    // flex: 1,
    // flexDirection: 'row',
    // // alignItems: 'flex-start',
    // // justifyContent: 'flex-start',
    // backgroundColor: 'green',
    // height: 200,
    paddingTop: 35,
    marginTop: 0,
    // paddingLeft: 10,
    // paddingRight: 10,
    // paddingBottom: 10,
  }
})
