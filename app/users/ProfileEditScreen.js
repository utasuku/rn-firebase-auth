import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  BackHandler
} from "react-native"

import Ionicons from 'react-native-vector-icons/Ionicons'

import Firebase from '../../lib/firebase'
import { Container, Content, Header, Item, Right, Icon, Text, Fab, Toast,
  Form, Label, Input, DatePicker, Textarea, Picker } from 'native-base'


export default class ProfileEditScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Mi perfil',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      date: null,
      genre: '1',
      genreSelected: '1',
      chosenDate: new Date(),
    }

    this.setDate = this.setDate.bind(this)
  }

  setDate = (newDate) => {
    this.setState({
      date: newDate,
    })
  }

  onGenreChange(value: string) {
    this.setState({
      genre: value
    })
  }

  _retrieveData = () => {

  }

  _getData = () => {

  }

  _onUpdate = () => {

  }

  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Nombres</Label>
              <Input />
            </Item>
            <Item stackedLabel>
              <Label>Apellidos</Label>
              <Input />
            </Item>
            <Item>
              <Ionicons name="md-calendar" size={24}/>
              <DatePicker
                defaultDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={'Selecciona la fecha...'}
                textStyle={{ color: "green", textAlign: 'left' }}
                placeHolderTextStyle={{ color: "#ccc", textAlign: 'left' }}
                onDateChange={this.setDate}
              />
            </Item>
            <Item stackedLabel>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item>
              <Picker
                note
                mode="dropdown"
                style={{ width: 120 }}
                selectedValue={this.state.genre}
                onValueChange={this.onGenreChange.bind(this)}
              >
                <Picker.Item label="Hombre" value="1" />
                <Picker.Item label="Mujer" value="2" />
              </Picker>
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }

  componentDidMount() {

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
  }
});
