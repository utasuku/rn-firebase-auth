import React, { Component } from "react";
import {
    View,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Image,
    TouchableOpacity,
    TouchableHighlight,
} from "react-native"
import { Container, Content, Left, Header, Icon, Text, Thumbnail } from 'native-base'

import { createStackNavigator, createDrawerNavigator, DrawerItems } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'

import AppTabNavigator from './HomeScreenTabNavigator'
import SearchScreen from './SearchScreen'
import GeneralScreen from './GeneralScreen'
import AllergyScreen from './AllergyScreen'
import BodyExploreScreen from './BodyExploreScreen'
import BodyExploreDetailScreen from './body-explore/BodyExploreDetailScreen'
import BodyExploreAddScreen from './body-explore/BodyExploreAddScreen'

import HistoryScreen from './history/HistoryScreen'
import HistoryAddScreen from './history/HistoryAddScreen'
import HistoryDetailScreen from './history/HistoryDetailScreen'
import SpendAddScreen from './history/SpendAddScreen'
import SpendDetailScreen from './history/SpendDetailScreen'
import DiagnosticAddScreen from './history/DiagnosticAddScreen'
import DiagnosticDetailScreen from './history/DiagnosticDetailScreen'
import LabsAddScreen from './history/LabsAddScreen'
import LabsDetailScreen from './history/LabsDetailScreen'

import SettingsScreen from './SettingsScreen'
import PrivacyScreen from './settings/PrivacyScreen'
import NotificationScreen from './settings/NotificationScreen'
import CameraScreen from '../components/Camera'

import ProfileViewScreen from './users/ProfileViewScreen'
import ProfileEditScreen from './users/ProfileEditScreen'

const HomeStackNavigator = createStackNavigator({
  HomeTab: AppTabNavigator,
  Search: SearchScreen,
  ProfileView: ProfileViewScreen,
})
const GeneralStackNavigator = createStackNavigator({ GeneralScreen })
const AllergyStackNavigator = createStackNavigator({ AllergyScreen })
const BodyExploreStackNavigator = createStackNavigator({
  BodyExplore: BodyExploreScreen,
  BodyExploreDetail: BodyExploreDetailScreen,
  BodyExploreAdd: BodyExploreAddScreen,
  BodyExplorePhoto: CameraScreen
})

const HistoryStackNavigator = createStackNavigator({
  History: HistoryScreen,
  HistoryAdd: HistoryAddScreen,
  HistoryDetail: HistoryDetailScreen,
  SpendAdd: SpendAddScreen,
  SpendDetail: SpendDetailScreen,
  DiagnosticAdd: DiagnosticAddScreen,
  DiagnosticDetail: DiagnosticDetailScreen,
  LabsAdd: LabsAddScreen,
  LabsDetail: LabsDetailScreen,
})

const SettingsStackNavigator = createStackNavigator({
  Settings: { screen: SettingsScreen },
  Privacy: { screen: PrivacyScreen },
  Notification: { screen: NotificationScreen }
},{
  initialRouteName: 'Settings',
})

const ProfileStackNavigator = createStackNavigator({
  ProfileEdit: { screen: ProfileEditScreen }
})

const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

const CustomDrawerComponent = (props) => (
  <Container>
    <Header style={{
      paddingTop: 25,
      backgroundColor: 'green',
      height: 100,
      borderBottomWidth: 5,
      borderBottomColor: '#f2f2f2'
    }}>
      <Left style={{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#f2f2f2'
      }}>
        <Thumbnail style={{
          // marginLeft: 10,
          marginRight: 10,
        }} source={{uri: uri}} />
        <View style={{ flexDirection: 'column' }}>
          <Text style={{ fontSize: 14, }}>Ulises García</Text>
          <Text style={{ fontSize: 12, }}>spirit.ike@outlook.com</Text>
        </View>
      </Left>
    </Header>
    <Content>
      <View style={{ height: 60, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
        <TouchableHighlight style={{
          // flex: 1,
          height: 50,
          width: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          borderColor: '#f2f2f2',
          borderWidth: 2
        }}>
          <Icon type="MaterialIcons" name="trending-up" />
        </TouchableHighlight>
        <TouchableHighlight style={{
          // flex: 1,
          height: 50,
          width: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          borderColor: '#f2f2f2',
          borderWidth: 2
        }}>
          <Icon type="MaterialIcons" name="favorite" />
        </TouchableHighlight>
        <TouchableHighlight style={{
          // flex: 1,
          height: 50,
          width: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          borderColor: '#f2f2f2',
          borderWidth: 2
        }}>
          <Icon type="MaterialIcons" name="person" />
        </TouchableHighlight>
        <TouchableHighlight style={{
          // flex: 1,
          height: 50,
          width: 50,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          borderColor: '#f2f2f2',
          borderWidth: 2
        }}>
          <Icon type="MaterialIcons" name="local-hospital" />
        </TouchableHighlight>
      </View>
      <ScrollView>
        <DrawerItems {...props} />
      </ScrollView>
    </Content>
  </Container>
  // <SafeAreaView style={{ flex: 1 }}>
  //   <View style={{ height: 150, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center' }}>
  //     <Image source={require('../assets/mf_small.png')} style={{ height: 120, width: 200, borderRadius: 60 }} />
  //   </View>
  //   <ScrollView>
  //     <DrawerItems {...props} />
  //   </ScrollView>
  // </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeStackNavigator,
    navigationOptions: {
      drawerLabel: 'Inicio',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-home" size={24} />
      ),
    }
  },
  General: {
    screen: GeneralStackNavigator,
    navigationOptions: {
      drawerLabel: 'Generales',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-heart" size={24} />
      ),
    }
  },
  BodyExploration: {
    screen: BodyExploreStackNavigator,
    navigationOptions: {
      drawerLabel: 'Exploración física',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-body" size={24} />
      ),
    }
  },
  Allergy: {
    screen: AllergyStackNavigator,
    navigationOptions: {
      drawerLabel: 'Alergías',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-list" size={24} />
      ),
    }
  },
  History: {
    screen: HistoryStackNavigator,
    navigationOptions: {
      drawerLabel: 'Historial médico',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-clipboard" size={24} />
      ),
    }
  },
  Profile: {
    screen: ProfileStackNavigator,
    navigationOptions: {
      drawerLabel: 'Mi perfil',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="ios-contact" size={24} />
      ),
    }
  },
  Settings: {
    screen: SettingsStackNavigator,
    navigationOptions: {
      drawerLabel: 'Ajustes',
      drawerIcon: ({ tintColor }) => (
        <Ionicons name="md-settings" size={24} />
      ),
    }
  }
},{
  initialRouteName: 'Home',
  drawerPosition: 'left',
  contentComponent: CustomDrawerComponent,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
})


export default AppDrawerNavigator;
