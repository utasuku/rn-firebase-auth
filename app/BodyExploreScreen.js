import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  ListView,
  FlatList,
  TouchableHighlight,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Firebase from '../lib/firebase';

import { Input } from '../components/Input';
import { Button } from '../components/Button';

import { List, ListItem, Left, Body, Right, Icon, Fab, Text } from 'native-base';

import * as moment from 'moment-timezone';

var EXP_LIMIT = 15;

export default class BodyExploreScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Exploración física',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  constructor (props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      userLogged: false,
      loading: true,
      reloading: false,
      loadingMore: false,
      loadingStart: true,
      active: true,
      clicked: false,
      listViewData: [],
      page: 1,
      isZero: false,
      refreshing: false,
      startAfter: '',
    }

    this.db_explore = Firebase.firestore.collection("exploration");
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  reloadData () {
    this.db_explore
    .where('user_patient', '==', this.state.userLogged.uid)
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .limit(EXP_LIMIT)
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length;
      let dats = []

      if (count) {
        var lastVisible = snapshot.docs[count - 1];
        this.setState({startAfter: lastVisible})

        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD-MM-YYYY HH:mm")

          let tmpData = {
            id: doc.id,
            title: doc.data().title,
            frequently: doc.data().frequently,
            height: doc.data().height,
            updated_at: datetmp,
          }

          dats.push(tmpData)
        })
      }

      this.setState(state => ({
        listViewData: dats,
        loading: false,
        refreshing: false,
        loadingMore: false,
        modalVisible: false,
        isZero: (count < EXP_LIMIT) ? true: false,
      }))
    })
  }

  getData = async () => {
    this.db_explore
    .where('user_patient', '==', this.state.userLogged.uid)
    .where('active', '==', true)
    .orderBy("updated_at", "desc")
    .startAfter(this.state.startAfter)
    .limit(EXP_LIMIT)
    .get()
    .then((snapshot) => {
      let count = snapshot.docs.length;
      let dats = []

      if (count) {
        var lastVisible = snapshot.docs[count - 1];
        this.setState({startAfter: lastVisible})

        snapshot.docs.forEach(doc => {
          let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
          .format("DD-MM-YYYY HH:mm")

          let tmpData = {
            id: doc.id,
            title: doc.data().title,
            frequently: doc.data().frequently,
            height: doc.data().height,
            updated_at: datetmp,
          }

          dats.push(tmpData)
        })
      }

      this.setState(state => ({
        listViewData: [...this.state.listViewData, ...dats],
        loadingMore: false,
        modalVisible: false,
        isZero: (count < EXP_LIMIT) ? true: false,
      }))
    })
  }

  componentWillMount () {
    this._retrieveData().then(() => {
      this.getData().then(() => {
        setTimeout(() => {
          this.setState({
            loading: false,
            loadingStart: false,
          })
        }, 1500)
      });
    })
  }

  componentDidMount () {

  }

  componentWillUpdate () {

  }

  componentDidUpdate (prevProps, prevState) {
    const { navigation } = this.props;
    const reloadList = navigation.getParam('reloadList', false);

    if (reloadList && !this.state.reloading && !prevState.reloading) {
      this.setState({
        listViewData: [],
        reloading: true,
        startAfter: ''
      }, () => {
        this.reloadData();
      })
    }
  }

  _keyExtractor = (item, index) => item.id;

  // _renderItem = ({item}) => (
  //   <TouchableOpacity style={styles.itemList}>
  //     <View>
  //       <Text style={{ color: 'blue' }}>
  //         {item.title}
  //       </Text>
  //       <Text>Última actualización: {item.updated_at}</Text>
  //     </View>
  //   </TouchableOpacity>
  // );

  onPressDetail (item) {
    if (this.state.clicked) {
      return false
    }

    this.setState({clicked: true})
    this.props.navigation.push('BodyExploreDetail', {
      id: item.id,
      title: item.title,
    })

    setTimeout(() => {
      this.setState({clicked: false})
    }, 1000);
  }

  handleRefresh = () => {
    if (this.state.refreshing) {
      return false;
    }

    this.setState({
      refreshing: true,
      startAfter: ''
    }, () => {
      this.reloadData();
    })
  }

  handleEnd = () => {
    if (this.state.loadingMore || this.state.isZero || this.state.loadingStart) {
      return false
    }

    this.setState({
      loadingMore: true,
    },
    () => {
      this.getData()
    })
  }

  renderListFooter = () => {
    if (this.state.loadingMore) {
      return <ActivityIndicator style={styles.loadingMore} animating />
    }

    return (
      <View>
        <Button onPress={() => this.handleEnd() }>Ver más</Button>
      </View>
    )
  }

  render () {
    if (this.state.loading || this.state.loadingStart) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.listViewData}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => (
            <ListItem onPress={() => this.onPressDetail(item) }>
              <Body>
                <Text>{item.title}</Text>
                <Text note style={styles.italic}>Última actualización {item.updated_at}.</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          )}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          // ListFooterComponent={() => this.renderListFooter()}
          ListFooterComponent={() => {
              if (this.state.loadingMore) {
                return <ActivityIndicator style={styles.loadingMore} animating />
              } else {
                return null
              }
            }
          }
          onEndReached={() => this.handleEnd()}
          onEndReachedThreshold={0.5}
        />

        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            this.props.navigation.push('BodyExploreAdd')
          }}>
          <Icon name="add" />
        </Fab>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  scroll: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  italic: {
    fontStyle: 'italic'
  },
  loadingMore: {
    paddingTop: 15,
    paddingBottom: 15,
  },
  itemList: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderBottomColor: '#CCC',
    borderBottomWidth: 1,
  },
})
