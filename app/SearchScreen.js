import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  BackHandler,
  FlatList,
  ActivityIndicator,
  TouchableHighlight,
} from "react-native"

import Firebase from '../lib/firebase'
import { Container, Content, Header, Body, Left, Right, Thumbnail, Item, List, ListItem, Icon, Input,Text, Button } from 'native-base'
import { SearchBar } from 'react-native-elements'
import * as moment from 'moment-timezone'

const URI = '../assets/user.png'
const EXP_LIMIT = 15

export default class SearchScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <SearchBar
        showLoading={true}
        onChangeText={navigation.getParam('onChangeText')}
        placeholder='Search...'
        containerStyle={{
          backgroundColor: '#FFF',
          width: '100%',
        }}
        inputStyle={{
          backgroundColor: '#f2f2f2',
        }}
        />
      ),
      headerRight: (
        <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('onSearch')}>
          <Icon type="MaterialIcons" name="search"></Icon>
        </TouchableHighlight>
      ),
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      loading: true,
      searching: false,
      userLogged: null,

      search: '',
      startAfter: '',
      listProfiles: []
    }

    this.db_users = Firebase.firestore.collection("users")
  }

  _keyExtractor = (item, index) => item.id;

  _retrieveData = async () => {

  }

  _onChangeText = (search) => this.setState({search})

  _onSearch = async () => {
    this.setState({
      searching: true,
    }, () => {
      // console.log('searching...', this.state.search)
      let dats = []
      let dats_ni = []

      this.db_users
      .orderBy("name", "asc")
      .where('name', '>=', this.state.search)
      .where('active', '==', true)
      // .startAfter(this.state.startAfter)
      .limit(EXP_LIMIT)
      .get()
      .then((snapshot) => {
        let count = snapshot.docs.length

        if (count) {
          snapshot.docs.forEach(doc => {
            // console.log(doc.data())
            let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
            .format("DD-MM-YYYY HH:mm")

            let tmpData = {
              id: doc.id,
              uid: doc.data().uid,
              name: doc.data().name,
              lastname: doc.data().lastname,
              image: doc.data().image,
              username: doc.data().username,
              updated_at: datetmp,
            }

            dats.push(tmpData)
          })
        }

        this.db_users
        .orderBy("username", "asc")
        .where('username', '>=', this.state.search)
        .where('active', '==', true)
        .limit(EXP_LIMIT)
        .get()
        .then((snapshot) => {
          let count = snapshot.docs.length

          if (count) {
            snapshot.docs.forEach(doc => {
              // console.log(doc.data())
              let datetmp = moment.unix(doc.data().updated_at.seconds).tz('America/Mexico_City')
              .format("DD-MM-YYYY HH:mm")

              let tmpData = {
                id: doc.id,
                uid: doc.data().uid,
                name: doc.data().name,
                lastname: doc.data().lastname,
                image: doc.data().image,
                username: doc.data().username,
                updated_at: datetmp,
              }

              dats_ni.push(tmpData)
            })
          }

          // console.log('>> dats', dats)
          // console.log('>> dats_ni', dats_ni)

          var result = _.unionBy(dats_ni, dats, "id")

          this.setState(state => ({
            listProfiles: result,
            searching: false,
          }))
        }) // End Get

      }) // End Get
    }) // End SetState
  }

  renderProfiles = (profile) => {
    if (profile.image.length) {
      return (
        <ListItem avatar onPress={() => {
          this.props.navigation.navigate('ProfileView',{
            id: profile.id
          })
        }}>
          <Left>
            <Thumbnail source={{ uri: profile.image }} />
          </Left>
          <Body>
            <Text>{profile.name} {profile.lastname}</Text>
            <Text note style={styles.italic}>{profile.username}</Text>
          </Body>
          <Right>
            <Icon name="arrow-forward" />
          </Right>
        </ListItem>
      )
    }

    return (
      <ListItem avatar onPress={() => {
        this.props.navigation.navigate('ProfileView',{
          id: profile.id
        })
      }}>
        <Left>
          <Thumbnail source={require('../assets/user.png')} />
        </Left>
        <Body>
          <Text>{profile.name} {profile.lastname}</Text>
          <Text note style={styles.italic}>{profile.username}</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    )
  }

  render() {
    if (this.state.loading || this.state.searching) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    if (!this.state.listProfiles.length) {
      return (
        <Container style={styles.containerCenter}>
          <Text>Buscar perfiles</Text>
        </Container>
      )
    }

    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <FlatList
          data={this.state.listProfiles}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => this.renderProfiles(item)}
          />
        </Content>
      </Container>
    )
  }

  componentDidMount() {
    this._retrieveData().then(() => {
      this.setState({
        loading: false,
      })
    })

    this.props.navigation.setParams({ onChangeText: this._onChangeText })
    this.props.navigation.setParams({ onSearch: this._onSearch })
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  content: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  searchBar: {
    width: '100%',
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
});
