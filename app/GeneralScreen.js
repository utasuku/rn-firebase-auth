import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  AsyncStorage
} from 'react-native';

import Firebase from '../lib/firebase';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Input } from '../components/Input';
import { Button } from '../components/Button';

export default class GeneralScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Generales',
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      userLogged: null,
      loading: true,
      head: '',
      neck: '',
      thorax: '',
      eyes: '',
      ears: '',
      pharynx: '',
      abdomen: '',
      others: '',
      generalId: null,
    }
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  saveData() {
    Firebase.firestore.collection('general_aspect')
    .doc(this.state.generalId)
    .update({
      head: this.state.head || '',
      neck: this.state.neck || '',
      thorax: this.state.thorax || '',
      eyes: this.state.eyes || '',
      ears: this.state.ears || '',
      pharynx: this.state.pharynx || '',
      abdomen: this.state.abdomen || '',
      others: this.state.others || '',
    })
  }

  getData() {
    Firebase.firestore.collection('general_aspect')
    .where('uid', '==', this.state.userLogged.uid)
    .limit(1)
    .get()
    .then((snapshot) => {
      let {
        head, neck, thorax, eyes, ears, pharynx, abdomen, others,
      } = this.state

      let id = null
      snapshot.docs.forEach(doc => {
        head = doc.data().head || ''
        neck = doc.data().neck || ''
        thorax = doc.data().thorax || ''
        eyes = doc.data().eyes || ''
        ears = doc.data().ears || ''
        pharynx = doc.data().pharynx || ''
        abdomen = doc.data().abdomen || ''
        others = doc.data().others || ''

        id = doc.id
      })

      this.setState({
        head: head,
        neck: neck,
        thorax: thorax,
        eyes: eyes,
        ears: ears,
        pharynx: pharynx,
        abdomen: abdomen,
        others: others,
        loading: false,
        generalId: id
      })
      // console.log(this.state)
    })
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.getData()
    })
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <ScrollView style={styles.scroll}>
          <Input
            placeholder='(cm)'
            label='Cabeza'
            onChangeText={head => this.setState({head})}
            value={this.state.head}
          />
          <Input
            placeholder='(cm)'
            label='Cuello'
            onChangeText={neck => this.setState({neck})}
            value={this.state.neck}
          />
          <Input
            placeholder='(cm)'
            label='Torax'
            onChangeText={thorax => this.setState({thorax})}
            value={this.state.thorax}
          />
          <Input
            placeholder=''
            label='Color de Ojos'
            onChangeText={eyes => this.setState({eyes})}
            value={this.state.eyes}
          />
          <Input
            placeholder=''
            label='Orejas'
            onChangeText={ears => this.setState({ears})}
            value={this.state.ears}
          />
          <Input
            placeholder='Comentarios extra'
            label='Otros'
            onChangeText={others => this.setState({others})}
            value={this.state.others}
            multiline={true}
          />
          <Button onPress={() => this.saveData() }>Guardar</Button>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  scroll: {
    paddingLeft: 20,
    paddingRight: 20,
  },
})
