import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Modal,
  Platform,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  ListView,
  Alert,
  AsyncStorage
} from 'react-native';

import Firebase from '../lib/firebase';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { Input } from '../components/Input';

import { List, ListItem, Left, Right, Icon, Fab, Button } from 'native-base';

export default class AllergyScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <View style={{ padding: 10 }}>
          <Ionicons name="md-menu" size={24} onPress={() => navigation.openDrawer()} />
        </View>
      )
    }
  }

  constructor (props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      userLogged: null,
      loading: true,
      loadingSave: false,
      modalVisible: false,
      newAllergy: '',
      active: true,
      basic: true,
      listViewData: [],
      errorSave: ''
    };

    this.db_allergies = Firebase.firestore.collection('allergies');
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  setModalVisible (visible) {
    this.setState({modalVisible: visible});
  }

  resetModal () {
    this.setState({
      newAllergy: '',
      modalVisible: false,
    })
  }

  deleteRow(secId, rowId, rowMap) {
    Alert.alert('Alert', 'Quieres eliminar?', [
      {text: 'Cancelar'},
      {text: 'Eliminar', onPress: () => {
        rowMap[`${secId}${rowId}`].props.closeRow();
        this.db_allergies.doc(this.state.listViewData[rowId].id)
        .delete().then(() => {
          const newData = [...this.state.listViewData];
          newData.splice(rowId, 1);
          this.setState({ listViewData: newData });
        }).catch((error) => {
          console.error("Error removing document: ", error);
        })
      }}
    ]),
    { cancelable: true}
  }

  getData() {
    this.db_allergies
    .where('uid', '==', this.state.userLogged.uid)
    .onSnapshot((snapshot) => {
      let allergies = []
      snapshot.docs.forEach(doc => {
        let tmpAllergy = {
          id: doc.id,
          title: doc.data().title,
        }

        allergies.push(tmpAllergy)
      })

      this.setState({
        listViewData: allergies,
        loading: false,
        modalVisible: false,
        loadingSave: false,
      })
    })
  }

  saveData () {
    this.setState({loadingSave: true})

    this.db_allergies.add({
      title: this.state.newAllergy,
      uid: this.state.userLogged.uid
    })
    .then(() => {
      // this.getData();
    })
    .catch((error) => {
      this.setState({
        errorSave: 'No se pudo crear el registro, intente nuevamente.',
        loadingSave: false,
      })
    });
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.getData()
    })
  }

  renderButtonSave () {
    if (this.state.loadingSave) {
       return (
         <ActivityIndicator size="large" />
       )
    } else {
      return (
        <Button full primary onPress={() => this.saveData() }>
          <Text>Agregar nuevo</Text>
        </Button>
      )
    }
  }

  renderModal () {
    return (
      <Modal
        style={styles.modal}
        position="center"
        backdrop={false}
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.resetModal()
        }}>
        <View style={styles.modalContent}>
          <TouchableHighlight style={styles.btnClose} onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
            <Ionicons style={styles.iconClose} name="md-close" size={30}/>
          </TouchableHighlight>
          <Input
            placeholder='Escribe tu alergía...'
            label='Alergía'
            onChangeText={newAllergy => this.setState({newAllergy})}
            value={this.state.newAllergy} />

            {this.renderButtonSave()}

           <Text style={styles.errorSave}>{this.state.errorSave}</Text>
        </View>
      </Modal>
    )
  }

  render () {
    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <List
          disableRightSwipe={true}
          rightOpenValue={-75}
          dataSource={this.ds.cloneWithRows(this.state.listViewData)}
          renderRow={data =>
            <ListItem>
              <Text> {data.title} </Text>
            </ListItem>}
          renderRightHiddenRow={(data, secId, rowId, rowMap) =>
            <Button full danger onPress={() => this.deleteRow(secId, rowId, rowMap)}>
              <Icon active name="trash" />
            </Button>}
        />

        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            this.setModalVisible(true)
          }}>
          <Icon name="add" />
        </Fab>

        {this.renderModal()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  item: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    fontSize: 18,
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#FFF',
  },
  btnClose: {
    alignItems: 'flex-end',
  },
  iconClose: {
    color: '#00aeef',
  },
  icon: {
    width: 24,
    height: 24,
  },
  modalContent: {
    padding: 20,
    margin: 20,
    backgroundColor: '#FFF',
    borderRadius: 4,
  },
  swipeContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  swipeContent: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'mediumseagreen',
  },
  swipeItem: {
    flex: 1,
    flexDirection: 'column',
    height: 100
  },
  swipeBlock: {
    height: 1,
    backgroundColor: 'white',
  },
  errorSave: {
    color: 'red',
    marginTop: 10,
    marginBottom: 10,
  }
});
