import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  AsyncStorage,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  Alert,
} from 'react-native';

import Firebase from '../../lib/firebase'
// import { Input } from '../components/Input';
import { Button } from '../../components/Button';
import { InputLabel } from '../../components/InputLabel';

import { Container, Content, Card, CardItem, Body, Text, H1, H2, H3,
  Form, Item, Input, Label, Textarea, Icon, DatePicker, ActionSheet,
  Drawer, Toast, Thumbnail } from 'native-base';

import * as moment from 'moment-timezone';

var BUTTONS = ['Editar', 'Eliminar','Cancelar'];
var DELETE_INDEX = 1;
var CANCEL_INDEX = 2;

const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

export default class BodyExploreDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // title: navigation.getParam('title', 'Detalle'),
      headerRight: (
        <TouchableHighlight style={styles.headerBtn} onPress={navigation.getParam('drawer')}>
          <Icon name="md-more"></Icon>
        </TouchableHighlight>
      ),
    }
  }

  constructor (props) {
    super(props);

    this.state = {
      userLogged: null,
      loading: true,
      loadingSave: false,
      loadingUpdating: false,
      errorUpdating: '',
      weight: '',
      imc: '',
      temperature: '',
      size_arm: '',
      size_leg: '',
      others: '',
      active: '',
      date: new Date(),
      dateNow: new Date(),
      user_patient: '',
      user_create: '',
      user_update: '',
    }

    const { navigation } = this.props;
    const id = navigation.getParam('id', '');

    this.db_explore = Firebase.firestore.collection('exploration').doc(id);
    this.setDate = this.setDate.bind(this);
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  setDate(newDate) {
    this.setState({ date: newDate });
  }

  getData () {
    this.db_explore
    .get()
    .then((doc) => {
      if (doc.exists) {

        let tmpData = doc.data()
        let dateDefault = new Date(
          moment.unix(tmpData.date.seconds).tz('America/Mexico_City').format("YYYY/MM/DD")
        );
        let dateDefaultNow =
          dateDefault.getDate() + '-'+ (dateDefault.getMonth() + 1) + '-' + dateDefault.getFullYear()

        this.setState({
          active: tmpData.active,
          date: dateDefault,
          dateNow: dateDefaultNow,
          weight: tmpData.weight,
          height: tmpData.height,
          imc: tmpData.imc,
          others: tmpData.others,
          size_arm: tmpData.size_arm,
          size_leg: tmpData.size_leg,
          temperature: tmpData.temperature,
          title: tmpData.title,
          active: tmpData.active,
          user_patient: tmpData.user_patient,
          user_create: tmpData.user_create,
          user_update: tmpData.user_update,
          created_at: tmpData.created_at,
          updated_at: tmpData.updated_at,
          loading: false,
          clicked: '',
        })

      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch((error) => {
      console.log("Error getting document:", error);
    });
  }

  onUpdate () {
    this.setState({loadingSave: true})
    let timestamp = new Date()

    this.db_explore.update({
      date: this.state.date,
      weight: this.state.weight,
      height: this.state.height,
      imc: this.state.imc,
      others: this.state.others,
      size_arm: this.state.size_arm,
      size_leg: this.state.size_leg,
      temperature: this.state.temperature,
      title: this.state.title,
      user_update: this.state.userLogged.uid,
      updated_at: timestamp
    })
    .then(() => {
      this.setState({loadingSave: false})
      Toast.show({
        text: 'Se ha actualizado correctamente',
        duration: 3000,
        type: 'success'
      });
    })
    .catch((error) => {
        // The document probably doesn't exist.ñ
        console.error("Error updating document: ", error);
    })
  }

  onDelete () {
    this.setState({
      loadingUpdating: true
    }, () => {
      let timestamp = new Date()

      this.db_explore.update({
        updated_at: timestamp,
        active: false,
      })
      .then(() => {
        this.setState({
          loadingUpdating: false
        }, () => {
          this.props.navigation.navigate('BodyExplore', {
            reloadList: true
          })
        })
      })
      .catch((error) => {
        this.setState({loadingUpdating: false})
        Toast.show({
          text: 'No se pudo eliminar, intente nuevamente',
          buttonText: 'Okay'
        })
      })
    })
  }

  renderButton () {
    if (this.state.loadingSave) {
      return (
        <ActivityIndicator size="large" />
      )
    }

    return (
      <Button onPress={() => this.onUpdate()}>Actualizar</Button>
    )
  }

  deleteExplore () {
    Alert.alert(
      '¿Eliminar registro?',
      '¿Deseas borrar este registro?',
      [
        {text: 'Cancelar', onPress: () => {
          console.log('Cancel Pressed');
        }, style: 'cancel'},
        {text: 'Sí, eliminar', onPress: () => {
          this.onDelete()
          console.log('OK Pressed')
        }},
      ],
      { cancelable: false }
    )
  }

  openDrawex = () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DELETE_INDEX,
        title: "Selecciona una opción"
      }, (buttonIndex) => {
        switch (buttonIndex) {
          case 0:
            break;
          case 1:
            this.deleteExplore()
            break;
          default:
            break;
        }
      })
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.getData();
    })

    this.props.navigation.setParams({ drawer: this.openDrawex });
  }

  render () {
    if (this.state.loading || this.state.loadingUpdating) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Body>
                <Text>Motivo de la exploración fisica</Text>
                <Text>{this.state.title}</Text>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <Form>
              <Item inlineLabel>
                <Label style={styles.label}>Peso (kg)</Label>
                <Input onChangeText={weight => this.setState({weight})}>{this.state.weight}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.label}>Altura (cm)</Label>
                <Input onChangeText={height => this.setState({height})}>{this.state.height}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.label}>IMC</Label>
                <Input onChangeText={imc => this.setState({imc})}>{this.state.imc}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.label}>Temperatura (°C)</Label>
                <Input onChangeText={temperature => this.setState({temperature})}>{this.state.temperature}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.label}>Medida brazo</Label>
                <Input onChangeText={size_arm => this.setState({size_arm})}>{this.state.size_arm}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.label}>Medida pierna</Label>
                <Input onChangeText={size_leg => this.setState({size_leg})}>{this.state.size_leg}</Input>
              </Item>
              <Item inlineLabel>
                <Label style={styles.labelSubtitle}>Fecha de visita</Label>
              </Item>
              <Item>
                <Icon active name='md-calendar' />
                <DatePicker
                  defaultDate={new Date(this.state.date)}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText={this.state.dateNow}
                  textStyle={{ color: "green" }}
                  placeHolderTextStyle={{ color: "#d3d3d3" }}
                  onDateChange={this.setDate}
                  />
              </Item>
              <InputLabel
                placeholder=''
                label='Comentarios extra'
                onChangeText={others => this.setState({others})}
                value={this.state.others}
                multiline={true}
                numberOfLines={3}
              />

              <View style={styles.footer}>
                {this.renderButton()}
              </View>
            </Form>
          </Card>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  label: {
    fontSize: 12,
    width: 120,
  },
  labelSubtitle: {
    fontSize: 12,
    paddingTop: 15,
    paddingBottom: 10,
  },
  textarea: {
    flex: 1,
  },
  contentImages: {
    padding: 10,
  },
  titleImage: {
    color: '#00aeef',
    marginTop: 10,
    marginBottom: 10,
  },
  scroll: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  footer: {
    padding: 10,
  },
  headerBtn: {
    marginRight: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F00'
  }
})
