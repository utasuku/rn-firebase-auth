import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  AsyncStorage,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
  TouchableOpacity
} from 'react-native';

import Firebase from '../../lib/firebase'

import { Button } from '../../components/Button';
import { InputLabel } from '../../components/InputLabel';
// import { CameraScreen } from '../../components/Camera'

import { Camera, Permissions } from 'expo';

import { Container, Content, Card, CardItem, Body, Text, H1, H2, H3,
  Form, Item, Input, Label, Textarea, Icon, DatePicker, Thumbnail } from 'native-base';

const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

export default class BodyExploreAddScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Nuevo registro'
    }
  }

  constructor (props) {
    super(props);

    this.state = {
      loading: true,
      loadingSave: false,
      userLogged: null,
      title: '',
      weight: '',
      height: '',
      imc: '',
      temperature: '',
      size_arm: '',
      size_leg: '',
      others: '',
      active: false,
      user_patient: '',
      user_create: '',
      user_update: '',

      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
    }

    this.db_explore = Firebase.firestore.collection('exploration');
    this.setDate = this.setDate.bind(this);
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('_lsUser');
      if (value !== null) {
        this.setState({userLogged: JSON.parse(value)})
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  resetData () {
    this.setState({
      loadingSave: false,
      title: '',
      weight: '',
      height: '',
      imc: '',
      temperature: '',
      size_arm: '',
      size_leg: '',
      others: '',
      date: '',
      active: false,
      user_patient: '',
      user_create: '',
      user_update: '',
    })
  }

  componentDidMount () {
    this._retrieveData().then(() => {
      this.setState({
        loading: false,
      })
    })
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  setDate(newDate) {
    this.setState({ date: newDate });
  }

  saveData () {
    if (this.state.title == '' ||
      !this.state.date) {
      Alert.alert(
        'Campo faltante',
        'El campo del título y fecha es obligatorio',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )

      return false
    }

    this.setState({
      loadingSave: true
    }, () => {
      let timestamp = new Date()

      this.db_explore.add({
        title: this.state.title,
        weight: this.state.weight,
        height: this.state.height,
        imc: this.state.imc,
        temperature: this.state.temperature,
        size_arm: this.state.size_arm,
        size_leg: this.state.size_leg,
        others: this.state.others,
        date: this.state.date,
        active: true,
        user_patient: this.state.userLogged.uid,
        user_create: this.state.userLogged.uid,
        user_update: this.state.userLogged.uid,
        updated_at: timestamp,
        created_at: timestamp,
      })
      .then(() => {
        this.resetData()
        this.props.navigation.navigate('BodyExplore', {
          reloadList: true
        })
      })
      .catch((error) => {
        this.setState({
          // errorSave: 'No se pudo crear el registro, intente nuevamente.',
          loadingSave: false,
        })
      })
    })
  }

  renderButton () {
    if (this.state.loadingSave) {
      return (
        <ActivityIndicator size="large" />
      )
    }

    return (
      <Button onPress={() => this.saveData() }>Agregar</Button>
    )
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.containerCenter}>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    return (
      <Container style={styles.container}>
        <Content>
          <Card>
            <Form>
              <InputLabel
                placeholder=''
                label='Ingresa el motivo del registro'
                onChangeText={title => this.setState({title})}
                value={this.state.title}
              />
              <Item>
                <Icon active name='md-body' />
                <Input onChangeText={weight => this.setState({weight})}
                  placeholder='Peso (Kg)' >{this.state.weight}</Input>
              </Item>
              <Item>
                <Icon active name='ios-body' />
                <Input onChangeText={height => this.setState({height})}
                  placeholder='Altura (cm)'>{this.state.height}</Input>
              </Item>
              <Item>
                <Icon active name='ios-man' />
                <Input onChangeText={imc => this.setState({imc})}
                  placeholder='IMC'>{this.state.imc}</Input>
              </Item>
              <Item>
                <Icon active name='ios-thermometer' />
                <Input onChangeText={temperature => this.setState({temperature})}
                  placeholder='Temperatura (°C)'>{this.state.temperature}</Input>
              </Item>
              <Item>
                <Icon active name='md-clipboard' />
                <Input onChangeText={size_arm => this.setState({size_arm})}
                  placeholder='Medida brazo'>{this.state.size_arm}</Input>
              </Item>
              <Item>
                <Icon active name='md-clipboard' />
                <Input onChangeText={size_leg => this.setState({size_leg})}
                  placeholder='Medida pierna'>{this.state.size_leg}</Input>
              </Item>
              <Item>
                <Icon active name='md-calendar' />
                <DatePicker
                  defaultDate={new Date()}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText="Fecha de visita"
                  textStyle={{ color: "green" }}
                  placeHolderTextStyle={{ color: "#d3d3d3" }}
                  onDateChange={this.setDate}
                  />
              </Item>
              <InputLabel
                placeholder=''
                label='Comentarios extra'
                onChangeText={others => this.setState({others})}
                value={this.state.others}
                multiline={true}
                numberOfLines={3}
              />

              <View style={styles.contentImages}>
                <Text style={styles.titleImage}>Imagenes</Text>
                <Button onPress={() => {
                  this.props.navigation.navigate('BodyExplorePhoto')
                }}>
                  <Icon style={{color: '#FFF'}} active name='md-camera' />
                </Button>
                <Thumbnail large source={{uri: uri}} />
              </View>

              <View style={styles.footer}>
                {this.renderButton()}
              </View>
            </Form>
          </Card>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  contentImages: {
    padding: 10,
  },
  titleImage: {
    color: '#00aeef',
    marginTop: 10,
    marginBottom: 10,
  },
  footer: {
    padding: 10,
  }
})
